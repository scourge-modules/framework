﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ObjectPool<T> : ScriptableObjectBase where T : Object
{
    public int MaxAmount = 10; //TODO convert to references?
    public int InitialAmount = 5;
    public bool CanGrow = true;
    public bool ForceRecycle = false;

    private int index = 0;
    protected List<T> pooledObjects = new List<T>();

    public T this[int i]
    {
        get { return pooledObjects[i]; }
        set { pooledObjects[i] = value; }
    }


    
	public virtual void Initialize()
	{
        DestroyAll();
        index = 0;

        for (int i = 0; i < InitialAmount; i++)
		{
			pooledObjects.Add( MakeObject() );
		}
	}

    public virtual void DestroyAll()
    {
        for (int i = pooledObjects.Count-1; i >= 0; i--)
        {
            Destroy(pooledObjects[i]);
        }

        pooledObjects.Clear();
    }

	public T GetObject()
	{
		for (int i =0; i< pooledObjects.Count; i++)
		{
			if (!IsActive(pooledObjects[i]))
			{
				return pooledObjects[i];
			}
		}

		if (CanGrow && pooledObjects.Count < MaxAmount )
		{
			T t = MakeObject();
            pooledObjects.Add(t);
            return t;
		}

        if (ForceRecycle)
        {
            T t = pooledObjects[index];
            SetActiveState(t, false);
            index++;
            if (index >= pooledObjects.Count)
            {
                index = 0;
            }
            return t;
        }

		return null;
	}

	protected abstract T MakeObject();
	protected abstract bool IsActive(T pooledObject);
    protected abstract void SetActiveState(T pooledObject, bool active);
}
