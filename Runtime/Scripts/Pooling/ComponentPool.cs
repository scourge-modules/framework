﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Instantiating a component is the same as instantiating its GameObject, but returns a component, not a GameObject 
// so we avoid costly GetComponent<>() for initialization, assuming you only need to initialize one thing. 

[System.Serializable]
public class ComponentPool<T> : ObjectPool<T> where T : Component
{
	public T PrefabComponent;
    	
	protected Transform containerTransform;

	public void Initialize(Transform container=null)
	{
        if (container)
            containerTransform = container;
        else
    		containerTransform = new GameObject("Pooled " + typeof(T).Name).transform;

        base.Initialize();
	}

    public override void DestroyAll()
    {
        if (pooledObjects == null) pooledObjects = new List<T>();

        for (int i = pooledObjects.Count - 1; i >= 0; i--)
        {
            Destroy(pooledObjects[i].gameObject);
        }

        pooledObjects.Clear();
    }

    protected override T MakeObject()
	{
		T t = (T) MonoBehaviour.Instantiate(PrefabComponent);

        SetActiveState(t, false);
		t.transform.parent=containerTransform;
		
		return t;
	}

	protected override bool IsActive(T pooledComponent)
	{
		return pooledComponent.gameObject.activeInHierarchy;
	}

    protected override void SetActiveState(T pooledObject, bool active)
    {
        pooledObject.gameObject.SetActive(active);
    }

    public T TryProduce(Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
	{
		T t = GetObject();
		if (t != null)
		{
            SetActiveState(t, true);
            t.transform.position = position;
            t.transform.rotation = rotation;
        }
		return t;
	}

	public void HideAll()
	{
		foreach (T t in pooledObjects)
		{
            SetActiveState(t, false);
		}
	}

}
