﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializableDateTime : ISerializationCallbackReceiver
{
    [SerializeField] private int year;
    [SerializeField] private int month;
    [SerializeField] private int day;
    [SerializeField] private int hour;
    [SerializeField] private int minute;
    [SerializeField] private int second;
    [SerializeField] private int millisecond;
    [SerializeField] private DateTimeKind kind;

    [SerializeField] private string _stringRepresentation = "";

    public DateTime Value;

    public void OnBeforeSerialize()
    {
        if (Value != null)
        {
            year = Value.Year;
            month = Value.Month;
            day = Value.Day;
            hour = Value.Hour;
            minute = Value.Minute;
            second = Value.Second;
            millisecond = Value.Millisecond;
            kind = Value.Kind;
            _stringRepresentation = string.Format("{0}:{1}:{2} {3}/{4}/{5}", hour, minute, second, day, month, year);
        }
    }

    public SerializableDateTime(DateTime dateTime)
    {
        Value = dateTime;
        year = Value.Year;
        month = Value.Month;
        day = Value.Day;
        minute = Value.Minute;
        second = Value.Second;
        millisecond = Value.Millisecond;
        kind = Value.Kind;
    }

    public void OnAfterDeserialize()
    {
        Value = new DateTime(year, month, day, hour, minute, second, millisecond, kind);
    }

    public static implicit operator DateTime(SerializableDateTime reference) //Why doesnt this work?
    {
        return reference.Value;
    }
}
