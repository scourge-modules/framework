﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/DateTime")]
public class ScriptableDateTime : VariableObject<SerializableDateTime>
{

}
