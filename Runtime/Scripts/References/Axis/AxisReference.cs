﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AxisReference
{
    public int index;
    public ScriptableAxis Value;

    public bool Down => Value.ButtonDown;
    public bool Up => Value.ButtonUp;
    public bool Hold => Value.Button;

    public float Axis => Value.Axis;
    public string FriendlyName => Value.friendlyName;

}
