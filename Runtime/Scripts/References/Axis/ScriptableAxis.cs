﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/boo")]
public class ScriptableAxis : ScriptableString
{
    public string friendlyName;
    public bool ButtonDown => Input.GetButtonDown(Value);
    public bool ButtonUp => Input.GetButtonUp(Value);
    public bool Button => Input.GetButton(Value);
    public float Axis => Input.GetAxis(Value);

    protected override void OnEnable()
    {
        if (string.IsNullOrEmpty(friendlyName))
        {
            friendlyName = name;
        }
        base.OnEnable();
    }
}
