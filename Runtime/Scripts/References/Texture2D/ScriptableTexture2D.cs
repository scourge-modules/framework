using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Texture2D")]
public class ScriptableTexture2D : VariableObject<Texture2D>
{
    
}
