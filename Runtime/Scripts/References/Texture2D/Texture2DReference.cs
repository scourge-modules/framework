using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Texture2DReference : VariableReference<Texture2D, ScriptableTexture2D> 
{
}
