using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class ComponentListVariable : ComponentVariable
{

}
[ExecuteInEditMode]
public class ComponentListVariable<T> : ComponentVariable
{
	[SerializeField] private List<T> defaultValue;
	[SerializeField] private BoolReference shouldUpdateName;

	[SerializeField] private StringReference formatName;

	public List<T> Value;

	public override string ToString()
	{
		return Value.ToString();
	}

	public override string ToString(string format, IFormatProvider formatProvider)
	{
		if (Value is IFormattable)
			return (Value as IFormattable).ToString(format, formatProvider);
		else return Value.ToString();
	}

	private void Awake()
	{
		Value = defaultValue;
	}

	private void Update()
	{
		if (shouldUpdateName.Value)
		{
			name = string.Format(formatName, Value);
		}
	}
}
