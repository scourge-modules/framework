﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "New Static/Variable/Integer")]
public class ScriptableInt : VariableObject<int>
{
    [SerializeField] private BoolReference shouldFormatAddOne;
    public void SetValue(int i)
    {
        Value = i;
    }

    public void SetValue(Single s)
    {
        Value = (int)s;
    }

    public override string ToString(string format, IFormatProvider formatProvider)
    {
        if (shouldFormatAddOne.Value)
        {
            return (Value + 1 as IFormattable).ToString(format, formatProvider);
        }
        return (Value as IFormattable).ToString(format, formatProvider);
    }
}