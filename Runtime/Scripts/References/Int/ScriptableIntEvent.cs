﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "New Static/Event/IntEvent")]
public class IntEvent : GameEvent
{
    public new event Action<int> Event;

#if UNITY_EDITOR
    public int TestValue;

    [ContextMenu("Raise as Int")]
    public override void EditorRaise()
    {
        Raise(TestValue);
    }

#endif

    public void Raise(int value)
    {
        base.Raise();
        Debug.Log(name + " rasied with value: " + value);
        Event(value);
    }


}