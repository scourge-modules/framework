﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraReferenceInitializer : MonoBehaviour
{
    [SerializeField] private CameraReference reference;
    [SerializeField] private Camera cam;

    private void Awake()
    {
        reference.Value = cam;
    }
}
