﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Camera")]
public class ScriptableCamera : VariableObject<Camera>
{
}
