﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/List/Camera")]
public class ScriptableCameraList : ScriptableListPlus<Camera>
{
    public override void Insert(int index, Camera item)
    {
        if (index >= Count)
        {
            for (int i = 0; i < (index + 1) - Count; i++)
            {
                Add(null);
            }
        }
        if (this[index] != null)
        {
            Debug.LogError("Camera index already set, skipping!");
        }
        else
        {
            base.Insert(index, item);
        }
    }
}
