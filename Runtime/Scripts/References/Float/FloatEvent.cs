﻿using System;
using UnityEngine;

public class FloatEvent : GameEvent
{
	public new event Action<float> Event;

#if UNITY_EDITOR
	public float TestValue;

	[ContextMenu("Raise as Float")]
	public override void EditorRaise()
	{
		Raise(TestValue);
	}
#endif

	public void Raise(float value)
	{
		base.Raise();
		Event(value);
	}
}