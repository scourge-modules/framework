﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "New Static/Variable/Float")]
public class ScriptableFloat : VariableObject<float>
{
    public override string ToString(string format, IFormatProvider formatProvider)
    {
        return Value.ToString(format, formatProvider);
    }

    public void SetValue(float f)
    {
        Value = f;
    }

}