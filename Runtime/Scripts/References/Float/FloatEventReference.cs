﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class FloatEventReference : VariableReference
{
    public bool UseScriptable;
    public FloatEvent scriptable;
    public FloatUnityEvent variable;

    public void Raise(float f)
    {
        if (UseScriptable)
        {
            if (scriptable != null)
            {
                scriptable.Raise(f);
            }
            else
            {
                Debug.LogError("Event was null, skipping for now");
            }
        }
        else
        {
            variable.Invoke(f);
        }
    }
}
