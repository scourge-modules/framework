﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/RandomFloat")]
public class ScriptableRandomFloat : ScriptableFloat
{
    private float value;
    public override float Value => value = Random.value;
}
