﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Vector3")]
public class ScriptableVector3 : VariableObject<Vector3>
{

}
