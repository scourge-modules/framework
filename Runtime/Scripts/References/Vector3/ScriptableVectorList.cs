﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/List/Vector3")]
public class ScriptableVectorList : ScriptableList<Vector3>
{

    [System.Serializable]
    public class JSONVList
    {
        public List<Vector3> List;
    }

    [ContextMenu("Read from file")]
    public void ReadFromFile()
    {
        string path = "C:\\Users\\owenm\\Desktop\\Untitled-1.json";
        path = System.IO.File.ReadAllText(path);
        JSONVList svl = JsonUtility.FromJson<JSONVList>(path);
        List = svl.List;
    }
    [ContextMenu("Output as Log")]
    public void OutputAsLog()
    {
        // System.Text.StringBuilder sb = new System.Text.StringBuilder(JsonUtility.ToJson(this));
        string sb = JsonUtility.ToJson(this);

        largeLog(sb);
    }

    public void largeLog(string content)
    {
        if (content.Length > 4000)
        {
            Debug.Log(content.Substring(0, 4000));
            largeLog(content.Substring(4000));
        }
        else
        {
            Debug.Log(content);
        }
    }
}
