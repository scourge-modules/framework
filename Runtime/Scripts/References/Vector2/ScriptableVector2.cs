﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Vector2")]
public class ScriptableVector2 : VariableObject<Vector2>
{

}
