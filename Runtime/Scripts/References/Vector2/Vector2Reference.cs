﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Vector2Reference : VariableReference<Vector2, ScriptableVector2>
{

}