﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinnedMeshRootInitializer : MonoBehaviour
{
    [SerializeField] private Transform root;

    public void Spawn(SkinnedMeshRenderer renderer)
    {
        Instantiate(renderer, transform).rootBone = root;
    }
}
