﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "New Static/Event/BoolEvent")]
public class BoolEvent : GameEvent
{
    public new event Action<bool> Event;

#if UNITY_EDITOR
    public bool TestValue;

    [ContextMenu("Raise As Bool")]
    public override void EditorRaise()
    {
        Raise(TestValue);
    }
#endif

    public void Raise(bool value)
    {
        base.Raise();
        Event(value);
    }
}