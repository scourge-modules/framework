﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "New Static/Variable/Boolean")]
public class ScriptableBool : VariableObject<bool>
{
    public void SetValue(bool b)
    {
        Value = b;
    }
}