﻿using System;
using UnityEngine;


//TODO should have constructor to generate boolref, and +/- states

[CreateAssetMenu(menuName = "New Static/Event/Toggle Event")]
public class ToggleEvent : GameEvent
{
    [SerializeField] private BoolReference valueRef;
    public override void Raise()
    {
        valueRef.Value = !valueRef.Value;
        base.Raise();
    }
}