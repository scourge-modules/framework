using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ListReference 
{

}

public class ListReference<T> : ListReference
{
    public bool UseScriptable = false;
    public bool UseComponent = false;
    [SerializeField] protected List<T> variable;
    [SerializeField] protected ScriptableList<T> scriptable;
    [SerializeField] protected ComponentListVariable<T> component;

     public List<T> Value
    {
        get
        {
            if (UseComponent)
            {
                return component.Value;
            }
            return UseScriptable ? scriptable.List : variable;
        }
        set
        {
            if (UseComponent)
            {
                component.Value = value;
            }
            else
            {
                if (UseScriptable)
                {
                    scriptable.List = value;
                }
                else
                {
                    variable = value;
                }
            }
        }
    }
}
