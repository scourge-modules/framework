﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasInitializer : MonoBehaviour
{
    [SerializeField] private CameraReference cameraReference;
    [SerializeField] private Canvas canvas;

    private void Start()
    {
        canvas.worldCamera = cameraReference.Value;
    }
}
