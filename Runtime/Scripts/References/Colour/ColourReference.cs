﻿using System;
using UnityEngine;

[Serializable]
public class ColourReference : VariableReference<Color, ScriptableColour>
{
    
}