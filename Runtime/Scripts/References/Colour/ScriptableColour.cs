﻿using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Colour")]
public class ScriptableColour : VariableObject<Color>
{
}