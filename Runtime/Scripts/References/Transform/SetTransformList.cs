﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTransformList : MonoBehaviour
{
    [SerializeField] private ScriptableTransformList list;

    private void Awake()
    {
        list.Add(transform);
    }

    private void OnDestroy()
    {
        list.Remove(transform);
    }
}
