﻿using System;
using UnityEngine;

[Serializable]
public class TransformReference : VariableReference<Transform, ScriptableTransform>
{
    public Vector3 Position => Value.position;
    public Vector3 PositionNoY => new Vector3(Value.position.x, 0f, Value.position.z);
    public Quaternion Rotation => Value.rotation;
    public Vector3 Forward => Value.forward;
    public Vector3 ForwardNoY => new Vector3(Value.forward.x, 0f, Value.forward.z);
    public Vector3 Back => -Value.forward;
    public Vector3 Right => Value.right;
    public Vector3 Left => -Value.right;
    public Vector3 Up => Value.up;
    public Vector3 Down => -Value.up;
}
