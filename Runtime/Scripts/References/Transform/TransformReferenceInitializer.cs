﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformReferenceInitializer : MonoBehaviour
{
    [SerializeField] private ScriptableTransform target;

    private void Awake()
    {
        target.Value = transform;
    }
}
