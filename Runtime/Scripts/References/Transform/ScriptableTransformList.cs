﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/List/Transform")]
public class ScriptableTransformList : ScriptableList<Transform>
{
}
