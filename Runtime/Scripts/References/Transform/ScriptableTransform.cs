﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Transform")]
public class ScriptableTransform : VariableObject<Transform>
{
}
