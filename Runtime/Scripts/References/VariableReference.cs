﻿using System;
using System.Collections.Generic;
using UnityEngine;

//Use this class to allow ReferenceDrawer.cs to work for all references & for GameEventReference to use the same drawer
public abstract class VariableReference
{

}

public abstract class VariableReference<T, T2> : VariableReference where T2 : VariableObject<T>
{
    public bool UseScriptable = false;
    public bool UseComponent = false;
    [SerializeField] protected T variable;
    [SerializeField] protected T2 scriptable;
    [SerializeField] protected ComponentVariable<T> component;

    public T Value
    {
        get
        {
            if (UseComponent)
            {
                return component.Value;
            }
            return UseScriptable ? scriptable.Value : variable;
        }
        set
        {
            if (UseComponent)
            {
                component.Value = value;
            }
            else
            {
                if (UseScriptable)
                {
                    scriptable.Value = value;
                }
                else
                {
                    variable = value;
                }
            }
        }
    }

    public void SetValue(T value)
    {
        Value = value;
    }

    public VariableReference() { }

    public VariableReference(T value)
    {
        UseScriptable = false;
        Value = value;
    }

    public string ToString(string format, IFormatProvider formatProvider)
    {
        if (Value is IFormattable)
            return (Value as IFormattable).ToString(format, formatProvider);
        return ToString();

    }

    public override string ToString()
    {
        return Value.ToString();
    }

    public static implicit operator T(VariableReference<T, T2> reference)
    {
        return reference.Value;
    }

    public static implicit operator FormattableObject(VariableReference<T, T2> reference)
    {
        if (!reference.UseScriptable)
        {
            if (reference.scriptable == null)
            {
                reference.scriptable = ScriptableObject.CreateInstance<T2>();
            }
            T value = reference.Value;
            reference.scriptable.Value = value;
            reference.UseScriptable = true;
        }

        return reference.scriptable;
    }
}