﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "New Static/Variable/Condition")]
public class ScriptableCondition : VariableObject<Condition>
{

}