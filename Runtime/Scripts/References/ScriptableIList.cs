﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO this could all be made into references, and really all the abstract code from other stuff can get put into one file and asmdef'dinto a dll so we
// dont accidentally try to make things inherit from ScriptableIList - it should be internal and only ScriptableList avaliable
// should be ICollection anyway - should make corresponding values for all the c# collections, but particularly the main tree

public abstract class ScriptableIList : ScriptableObjectBase
{
    public abstract int Count { get; }

    //public abstract object this[int index] { get; set; }
}


public abstract class ScriptableList<T> : ScriptableIList, IList<T>
{
    public List<T> List;
    [SerializeField] private bool shouldClearOnPlayMode = false;

    //public override object this[int index] { get => ((IList<T>)List)[index]; set => ((IList<T>)List)[index] = value; }
    public virtual T this[int index] { get => ((IList<T>)List)[index]; set => ((IList<T>)List)[index] = value; }

    public bool IsReadOnly => ((IList<T>)List).IsReadOnly;

    public override int Count { get { return List.Count; } }

    public virtual void Add(T item)
    {
        ((IList<T>)List).Add(item);
    }

    public virtual void Clear()
    {
        ((IList<T>)List).Clear();
    }

    public bool Contains(T item)
    {
        return ((IList<T>)List).Contains(item);
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
        ((IList<T>)List).CopyTo(array, arrayIndex);
    }

    public IEnumerator<T> GetEnumerator()
    {
        return ((IList<T>)List).GetEnumerator();
    }

    public int IndexOf(T item)
    {
        return ((IList<T>)List).IndexOf(item);
    }

    public virtual void Insert(int index, T item)
    {
        ((IList<T>)List).Insert(index, item);
    }

    public virtual bool Remove(T item)
    {
        return ((IList<T>)List).Remove(item);
    }

    public virtual void RemoveAt(int index)
    {
        ((IList<T>)List).RemoveAt(index);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IList<T>)List).GetEnumerator();
    }

    public void Concat(IList<T> other)
    {
        List.Concatenate(other);
    }


#if UNITY_EDITOR
    protected override void HandlePlayModeChanged(UnityEditor.PlayModeStateChange change)
    {
        if (shouldClearOnPlayMode && change == UnityEditor.PlayModeStateChange.EnteredEditMode)
            Clear();
    }
#endif
}

public abstract class ScriptableListPlus<T> : ScriptableList<T>
{
    [SerializeField] private GameEventReference onChanged;

    public override T this[int index]
    {
        get => ((IList<T>)List)[index];
        set
        {
            ((IList<T>)List)[index] = value;
            if (onChanged != null)
                onChanged.Raise();
        }
    }

    public override void RemoveAt(int index)
    {
        base.RemoveAt(index);
        if (onChanged != null)
            onChanged.Raise();
    }
    public override void Add(T item)
    {
        base.Add(item);
        if (onChanged != null)
            onChanged.Raise();
    }
    public override void Clear()
    {
        base.Clear();
        if (onChanged != null)
            onChanged.Raise();
    }
    public override void Insert(int index, T item)
    {
        Debug.Log("INSERTING SHOULD TRIOGGER");
        base.Insert(index, item);
        if (onChanged != null)
            onChanged.Raise();
    }
    public override bool Remove(T item)
    {
        bool didRemove = base.Remove(item);
        if (onChanged != null)
            onChanged.Raise();
        return didRemove;
    }
}