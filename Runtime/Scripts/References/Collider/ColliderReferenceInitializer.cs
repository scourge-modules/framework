﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderReferenceInitializer : MonoBehaviour
{
    [SerializeField] private ScriptableCollider target;
    [SerializeField] private Collider thisCollider;

    private void Awake()
    {
        target.Value = thisCollider;
    }
}
