﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Collider")]
public class ScriptableCollider : VariableObject<Collider>
{
}
