﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "New Static/Event/GameEvent")]
public class GameEvent : ScriptableObject
{
    public bool shouldDebug;
    [TextArea] [SerializeField] private string note;
#if UNITY_EDITOR
    [ContextMenu("Raise")]
    public virtual void EditorRaise()
    {
        Raise();
    }
#endif

    public static GameEvent operator +(GameEvent trigger, Action action)
	{
		trigger.Event += action;
		return trigger;
	}
	public static GameEvent operator -(GameEvent trigger, Action action)
	{
		trigger.Event -= action;
		return trigger;
	}

    public event Action Event;

    public virtual void Raise()
    {
        if (Event != null)
        {
            if (shouldDebug)
            {
                Debug.Log(name + " rasied!");
            }
            Event();
        }
    }

    public int GetListenersCount()
    {
        if (Event == null)
            return 0;

        return Event.GetInvocationList().Length;
    }
}