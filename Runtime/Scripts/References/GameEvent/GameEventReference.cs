﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameEventReference : VariableReference
{
    public bool UseScriptable;
    public bool UseComponent;
    public GameEvent scriptable;
    public UnityEvent variable;
    public ComponentVariable<UnityEvent> component;

    public void Raise()
    {
        if (UseScriptable)
        {
            if (scriptable != null)
            {
                scriptable.Raise();
            }
            else
            {
                Debug.LogError("Event was null, skipping for now");
            }
        }
        else
        {
            variable.Invoke();
        }
    }

    public void Subscribe(UnityAction action)
    {
        if (UseScriptable)
        {
            scriptable.Event += action.Invoke;
        }
        else
        {
            variable.AddListener(new UnityAction(action));
        }
    }

    public void Unsubscribe(UnityAction action)
    {
        if (UseScriptable)
        {
            scriptable.Event -= action.Invoke;
        }
        else
        {
            variable.RemoveListener(new UnityAction(action));
        }
    }

    public int GetListenersCount()
    {
        if (UseScriptable)
        {
            return scriptable.GetListenersCount();
        }
        else
        {
            //This doesnt return number of events subscribed - ONLY the number serialized 
            return variable.GetPersistentEventCount();
        }
    }
}
