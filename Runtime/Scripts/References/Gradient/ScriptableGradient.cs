﻿using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Gradient")]
public class ScriptableGradient : VariableObject<Gradient>
{

}