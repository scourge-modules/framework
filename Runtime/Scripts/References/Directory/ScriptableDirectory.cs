﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Directory")]
public class ScriptableDirectory : VariableObject<SerializedDirectory>
{

}
