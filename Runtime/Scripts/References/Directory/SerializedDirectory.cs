﻿using UnityEngine;
using System.IO;
using UnityEditor;

[System.Serializable]
public class SerializedDirectory : ObjectReference<DirectoryInfo>
{
    public SerializedDirectory(DirectoryInfo directoryInfo)
    {
        Value = directoryInfo == null ? null : ConstructFromPath(directoryInfo.FullName);
    }

    public override DirectoryInfo ConstructFromPath(string path)
    {
        return string.IsNullOrEmpty(path) ? null : new DirectoryInfo(path);
    }

    public override string GetPathFromObject(DirectoryInfo info)
    {
        return info == null ? string.Empty : info.FullName;
    }
}
