﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class DirectoryReference : VariableReference<SerializedDirectory, ScriptableDirectory>
{
}