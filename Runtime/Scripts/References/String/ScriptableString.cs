﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "New Static/Variable/String")]
public class ScriptableString : VariableObject<string>
{
    //should trim setting
    public override string GetValueFromPlayerPref(string key)
    {
        string s = PlayerPrefs.GetString(key);
        return s;
    }

    public override void SetPlayerPrefFromValue(string key, string t)
    {
        PlayerPrefs.SetString(key, t);
    }
}