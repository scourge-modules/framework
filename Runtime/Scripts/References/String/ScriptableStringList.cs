﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/List/String")]
public class ScriptableStringList : ScriptableList<string>
{
}
