﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/List/Material")]
public class ScriptableMaterialList : ScriptableList<Material>
{

}
