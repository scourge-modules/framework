﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

//this is for any Asset we need represented as a string, like scenes or directories or files (maybe animations or audio snapshots?)
// TODO we still need to ensure all this is only happening in editor, as we assume the correct data has been serialized

public abstract class ObjectReference<T> : ISerializationCallbackReceiver where T : class
{
#if UNITY_EDITOR
    [SerializeField] private Object Asset = null; //UNITYENGINE.DEFAULTASSET
#endif
    [SerializeField] private string assetPath;

    public T Value;

    public abstract T ConstructFromPath(string path);
    public abstract string GetPathFromObject(T Object);

    public void OnBeforeSerialize() //Executed regularly
    {
#if UNITY_EDITOR //TODO
        if (Asset != null) // && !Application.isPlaying)
        {
            string path = AssetDatabase.GetAssetPath(Asset);
            assetPath.Replace("Assets/", "");
            path = Path.Combine(Application.dataPath.Replace("/Assets", ""), assetPath);
            if (assetPath != path)
            {
                AssetDatabase.Refresh();
                assetPath = GetPathFromObject(Value);
                //assetPath = Path.Combine(Application.dataPath, assetPath);
            }
        }
#endif
    }

    public void OnAfterDeserialize()
    {
#if UNITY_EDITOR
        if (!string.IsNullOrEmpty(assetPath))
        {
            Value = ConstructFromPath(assetPath);
        }
        EditorApplication.update += HandleAfterDeserialize;
#endif
    }
#if UNITY_EDITOR
    private void HandleAfterDeserialize()
    {
        EditorApplication.update -= HandleAfterDeserialize;

        if (Asset != null)
        {
            assetPath = AssetDatabase.GetAssetPath(Asset);
            assetPath = Path.Combine(Application.dataPath.Replace("/Assets", ""), assetPath);
        }

        if (assetPath != string.Empty)
        {
            Value = ConstructFromPath(assetPath);
            AssetDatabase.Refresh();
        }
    }

    public static implicit operator T(ObjectReference<T> reference) //Why doesnt this work?
    {
        return reference.Value;
    }
#endif
}

//public abstract class ScriptableClassSerializer<T> : ISerializationCallbackReceiver where T : ISerializationCallbackReceiver
//{
//    public abstract void OnAfterDeserialize();
//    public abstract void OnBeforeSerialize();
//}



// ScriptableDateTime : VariableObject <SerializableClass<DateTime>>



// a way of serializing objects that unity cant currently serialize