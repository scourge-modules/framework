﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/GameObject")]
public class ScriptableGameObjest : VariableObject<GameObject>
{
}
