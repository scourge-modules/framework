﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/List/GameObject")]
public class ScriptableGameObjectList : ScriptableList<GameObject>
{
}
