﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/Curve 3D")]
public class ScriptableCurve3D : VariableObject<Curve3D>
{
    [ContextMenu("Normalize")] //Todo Check?
    public void Normalize()
    {
        Value.Normalize();
    }
}
