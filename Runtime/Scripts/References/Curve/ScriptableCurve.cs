﻿using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/AnimationCurve")]
public class ScriptableCurve : VariableObject<AnimationCurve>
{

}