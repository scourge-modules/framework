﻿
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Curve3D //TODO this could be generic and work for any set of axis!
{
    public AnimationCurve X = new AnimationCurve();
    public AnimationCurve Y = new AnimationCurve();
    public AnimationCurve Z = new AnimationCurve();

    public Curve3D(Vector3[] fromPoints)
    {
        //X.Rebuild(fromPoints.Select(vec => vec.x).ToArray());
        //Y.Rebuild(fromPoints.Select(vec => vec.y).ToArray());
        //Z.Rebuild(fromPoints.Select(vec => vec.z).ToArray());
    }

    public int KeyCount
    {
        get { return Mathf.Max(X.keys.Length, Y.keys.Length, Z.keys.Length); }
    }

    public float Duration
    {
        get { return Mathf.Max(X.Duration(), Y.Duration(), Z.Duration()); }
    }

    public Vector3 Evaluate01(float t)
    {
        return new Vector3(X.Evaluate01(t), Y.Evaluate01(t), Z.Evaluate01(t));
    }

    public Vector3 Evaluate(float t)
    {
        return new Vector3(X.Evaluate(t), Y.Evaluate(t), Z.Evaluate(t));
    }

    public Vector3 GetRandom()
    {
        float t = UnityEngine.Random.Range(0f, Duration);

        return Evaluate(t);
    }

    public Vector3[] GetVectors(float timeStep, int count = 0)
    {
        count = (count <= 0) ? KeyCount : count;

        return Enumerable.Range(0, count).Select(x => new Vector3(X.Evaluate(timeStep * x), Y.Evaluate(timeStep * x), Z.Evaluate(timeStep * x))).ToArray();
    }

    public Vector3[] GetVectors(int count = 0)
    {
        return GetVectors(Duration / count, count);
    }

    [ContextMenu("Normalize")] //Todo Check?
    public void Normalize()
    {
        X.Normalize();
        Y.Normalize();
        Z.Normalize();
    }
}