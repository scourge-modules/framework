﻿using System;
using UnityEngine;

[Serializable]
public class CurveReference : VariableReference<AnimationCurve, ScriptableCurve>
{
    public float EvaluateLooped(float t)
    {
        t = Mathf.Abs(t) % Value.keys[Value.keys.Length - 1].time;
        return Value.Evaluate(t);
    }
}