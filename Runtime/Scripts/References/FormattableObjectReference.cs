using System;
using UnityEngine;

[Serializable]
public class FormattableObjectReference : VariableReference, IFormattable
{
	public bool UseScriptable = false;
	public bool UseComponent = false;
	[SerializeField] protected string variable;
	[SerializeField] protected FormattableObject scriptable;
	[SerializeField] protected ComponentVariable component;

	public string Value
	{
		get
		{
			if (UseComponent)
			{
				return component.ToString();
			}
			return UseScriptable ? scriptable.ToString() : variable;
		}
	}

	public string ToString(string format, IFormatProvider formatProvider)
	{
		return string.Format(formatProvider, Value);
	}
}
