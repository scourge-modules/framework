﻿using System.IO;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/FileInfo")]
public class ScriptableFileInfo : VariableObject<FileInfo>
{
}
