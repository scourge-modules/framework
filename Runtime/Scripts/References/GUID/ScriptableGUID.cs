﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/Variable/GUID")]
public class ScriptableGUID : ScriptableString
{
    protected override void OnEnable()
    {
        base.OnEnable();
        GenerateGUID();
    }

    public void GenerateGUID()
    {
        if (string.IsNullOrEmpty(Value))
        {
            Value = Guid.NewGuid().ToString();
        }
    }
}
