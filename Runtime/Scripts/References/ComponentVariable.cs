using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class ComponentVariable : MonoBehaviour, IFormattable
{
	public abstract override string ToString();
	public abstract string ToString(string format, IFormatProvider formatProvider);
}
[ExecuteInEditMode]
public class ComponentVariable<T> : ComponentVariable
{
	[SerializeField] private T defaultValue;
	[SerializeField] private BoolReference shouldUpdateName;

	[SerializeField] private StringReference formatName;
	public T Value;

	public override string ToString()
	{
		return Value.ToString();
	}

	public override string ToString(string format, IFormatProvider formatProvider)
	{
		if (Value is IFormattable)
			return (Value as IFormattable).ToString(format, formatProvider);
		else return Value.ToString();
	}

	public void SetValue(T value)
	{
		Value = value;
	}

	private void Awake()
	{
		Value = defaultValue;
	}

	private void Update()
	{
		if (shouldUpdateName.Value)
		{
			name = string.Format(formatName, Value);
		}
	}
}
