﻿using System;
using UnityEngine;


public abstract class VariableObject<T> : FormattableObject, IFormattable
{
    [SerializeField] private T defaultValue;
    [SerializeField] private T value;
    [SerializeField] private bool trackChanges;
    [SerializeField] private bool playerPrefChanges;
    [SerializeField] private bool logGet;
    [SerializeField] private bool logSet;
    [SerializeField] private bool shouldDefaultOnBuild;

    private T lastValue;

    public string PlayerPrefKey => name + "_" + GetInstanceID();

    public virtual T Value
    {
        get
        {
            if (playerPrefChanges)
            {
                if (PlayerPrefs.HasKey(PlayerPrefKey))
                {
                    value = GetValueFromPlayerPref(PlayerPrefKey);
                }
            }
            if (logGet)
            {
                Debug.Log("Getting value of " + name + " to be " + value);
            }
            return value;
        }
        set
        {
            if (trackChanges)
            {
                if (lastValue == null || !lastValue.Equals(value))
                {
                    onChanged.Raise();
                    lastValue = this.value;
                }

                onValueSet.Raise();
            }

            if (playerPrefChanges)
            {
                SetPlayerPrefFromValue(PlayerPrefKey, value);
            }

            if (logSet)
            {
                Debug.Log("Setting value of " + name + " to be " + value);
            }

            this.value = value;
        }
    }

    public virtual T GetValueFromPlayerPref(string key)
    {
        return default(T);
    }

    public virtual void SetPlayerPrefFromValue(string key, T t)
    {

    }

    public virtual T LastValue => lastValue;
    public virtual GameEventReference OnChanged => onChanged;
    public virtual GameEventReference OnValueSet => onValueSet;

    public override string ToString(string format, IFormatProvider formatProvider)
    {
        if (Value is IFormattable)
            return (Value as IFormattable).ToString(format, formatProvider);
        else return ToString();
    }

    public override string ToString()
    {
        return Value.ToString();
    }

    public static implicit operator T(VariableObject<T> scriptable)
    {
        return scriptable.Value;
    }

    public void ResetToDefault()
    {
        Value = defaultValue;
    }

#if UNITY_EDITOR
    protected override void HandlePlayModeChanged(UnityEditor.PlayModeStateChange change)
    {
        if (!PersistData && change == UnityEditor.PlayModeStateChange.ExitingPlayMode)
        {
            if (!playerPrefChanges)
            {
                ResetToDefault();
            }
        }
    }
#endif
}