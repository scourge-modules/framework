using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericGameEventReference<T, T2> : VariableReference<T, T2> where T2 : VariableObject<T>
{
        public event Action<T> Event;

#if UNITY_EDITOR
    public T TestValue;

    [ContextMenu("Raise as Int")]
    public void EditorRaise()
    {
        Raise(TestValue);
    }

#endif

    public void Raise(T value)
    {
        // base.Raise();
        // Debug.Log(name + " rasied with value: " + value);
        Event(value);
    }
}
