﻿using System;
using UnityEngine;

public abstract class FormattableObject : ScriptableObjectBase, IFormattable
{
    public abstract string ToString(string format, IFormatProvider formatProvider);


    [SerializeField] public GameEventReference onChanged;
    [SerializeField] public GameEventReference onValueSet;

#if UNITY_EDITOR
    [UnityEditor.MenuItem("CONTEXT/FormattableObject/GenerateScriptables", false, 2)]
    private static void GenerateScriptabales(UnityEditor.MenuCommand command)
    {
        FormattableObject formObj = (FormattableObject)command.context;

        GameEvent asset = ScriptableObject.CreateInstance<GameEvent>();
        asset.name = "On" + formObj.name + "Changed";
        UnityEditor.AssetDatabase.AddObjectToAsset(asset, formObj);
        formObj.onChanged.scriptable = asset;
        formObj.onChanged.UseScriptable = true;

        asset = ScriptableObject.CreateInstance<GameEvent>();
        asset.name = "On" + formObj.name + "Set";
        UnityEditor.AssetDatabase.AddObjectToAsset(asset, formObj);
        formObj.onValueSet.scriptable = asset;
        formObj.onValueSet.UseScriptable = true;

        UnityEditor.AssetDatabase.SaveAssets();
    }
    [UnityEditor.MenuItem("CONTEXT/FormattableObject/DestroyScriptables", false, 2)]
    private static void DestroyScriptabales(UnityEditor.MenuCommand command)
    {
        FormattableObject formObj = (FormattableObject)command.context;

        UnityEngine.Object.DestroyImmediate(formObj.onChanged.scriptable, true);
        UnityEngine.Object.DestroyImmediate(formObj.onValueSet.scriptable, true);

        formObj.onChanged.UseScriptable = false;
        formObj.onValueSet.UseScriptable = false;

        UnityEditor.AssetDatabase.SaveAssets();
    }

    public void GenerateOnSetScriptable()
    {
        GameEvent asset = ScriptableObject.CreateInstance<GameEvent>();

        UnityEditor.AssetDatabase.AddObjectToAsset(asset, this as UnityEngine.Object);
        onValueSet.scriptable = asset;
        onValueSet.UseScriptable = true;
        UnityEditor.AssetDatabase.SaveAssets();
    }
#endif
}