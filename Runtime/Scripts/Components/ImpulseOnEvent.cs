﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpulseOnEvent : MonoBehaviour
{
    public GameEvent EnableMoveEvent;
    public Vector3 Impulse;
    public Rigidbody Rigidbody;

    private void Awake()
    {
        EnableMoveEvent.Event += EnableMove;
    }

    private void OnDestroy()
    {
        EnableMoveEvent.Event -= EnableMove;
    }

    private void EnableMove()
    {
        Rigidbody.isKinematic = false;
        Rigidbody.AddForce(Impulse, ForceMode.Impulse);
    }

}
