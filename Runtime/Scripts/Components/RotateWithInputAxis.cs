﻿using UnityEngine;
using UnityEngine.Animations;

public class RotateWithInputAxis : MonoBehaviour
{
    public Vector3 Axis;
    public Space Space;
    public FloatReference Sensitivity;
    public FloatReference MaxConstraint;
    public StringReference AxisName;
    public BoolReference ShouldConstrain;
    public KeyCode Requirement;

    private float rotatedAmount;

    private void FixedUpdate()
    {
        if (Requirement != KeyCode.None && !Input.GetKey(Requirement))
            return;

        float amount = Time.deltaTime * Sensitivity * Input.GetAxis(AxisName);

        if (ShouldConstrain.Value)
        {
            if (Mathf.Abs(amount + rotatedAmount) > MaxConstraint)
            {
                amount = MaxConstraint - Mathf.Abs(rotatedAmount);
            }
        }

        rotatedAmount += amount;
        transform.Rotate(Axis, amount, Space);
    }
}
