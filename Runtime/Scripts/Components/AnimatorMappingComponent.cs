﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorMappingComponent : MonoBehaviour
{
    [SerializeField] private Animator animator;

    [System.Serializable]
    public class FloatMappingElement
    {
        public StringReference animName;
        public FloatReference valueRef;
    }

    [System.Serializable]
    public class BoolMappingElement
    {
        public StringReference animName;
        public BoolReference valueRef;
    }

    [System.Serializable]
    public class IntMappingElement
    {
        public StringReference animName;
        public IntReference valueRef;
    }

    [System.Serializable]
    public class TriggerMappingElement
    {
        public StringReference animName;
        public GameEvent valueRef;

        private Animator animator;

        public void Subscribe(Animator anim)
        {
            animator = anim;
            valueRef.Event += HandleEvent;
        }

        public void UnSubscribe()
        {
            valueRef.Event -= HandleEvent;
        }

        private void HandleEvent()
        {
            if (animator)
            {
                animator.SetTrigger(animName.Value);
            }
        }

    }

    [SerializeField] private List<TriggerMappingElement> triggerMap;
    [SerializeField] private List<BoolMappingElement> boolMap;
    [SerializeField] private List<IntMappingElement> intMap;
    [SerializeField] private List<FloatMappingElement> floatMap;

    private void Awake()
    {
        for (int i = 0; i < triggerMap.Count; i++)
        {
            triggerMap[i].Subscribe(animator);
        }
    }

    private void OnDestroy()
    {
        for (int i = 0; i < triggerMap.Count; i++)
        {
            triggerMap[i].UnSubscribe();
        }
    }

    private void Update()
    {
        for (int i = 0; i < floatMap.Count; i++)
        {
            animator.SetFloat(floatMap[i].animName, floatMap[i].valueRef.Value);
        }
        for (int i = 0; i < intMap.Count; i++)
        {
            animator.SetInteger(intMap[i].animName, intMap[i].valueRef.Value);
        }
        for (int i = 0; i < boolMap.Count; i++)
        {
            animator.SetBool(boolMap[i].animName, boolMap[i].valueRef.Value);
        }
    }
}
