﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOnEventCall : MonoBehaviour
{
    public GameEvent TriggerEvent;
	public BoolReference SetActive;
    public GameObject[] AffectedObjects;

    private void Awake()
    {
        TriggerEvent.Event += HandleTrigger;
    }

    private void OnDestroy()
    {
        TriggerEvent.Event -= HandleTrigger;
    }

    private void HandleTrigger()
    {
        for (int i=0; i<AffectedObjects.Length; i++)
        {
            AffectedObjects[i].SetActive(SetActive);
        }
    }
}
