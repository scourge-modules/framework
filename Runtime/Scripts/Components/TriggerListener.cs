﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerListener : MonoBehaviour
{
    [System.Serializable]
    public class TriggerType
    {
        public StringReference componentFilter;
        public GameEventReference OnEnter;
        public GameEventReference OnExit;
        public GameEventReference OnStay;
        public GameEventReference OnEnterOther;
        public GameEventReference OnExitOther;
        public GameEventReference OnStayOther;
    }

    [SerializeField] private List<TriggerType> types = new List<TriggerType>();

    private void OnTriggerEnter(Collider other)
    {
        foreach (TriggerType triggerType in types)
        {
            Component c = other.gameObject.GetComponent(triggerType.componentFilter);

            if (c)
            {
                triggerType.OnEnter.Raise();
            }
            else
            {
                triggerType.OnEnterOther.Raise();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        foreach (TriggerType triggerType in types)
        {
            Component c = other.gameObject.GetComponent(triggerType.componentFilter);

            if (c)
            {
                triggerType.OnExit.Raise();
            }
            else
            {
                triggerType.OnExitOther.Raise();
            }
        }
    }
}
