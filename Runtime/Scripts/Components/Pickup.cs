﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class Pickup : MonoBehaviour
{
    public StringReference Filter;
    public IntEvent EnterEvent;
    public GameObject VFX;
    public IntReference Value;

    private void OnTriggerEnter(Collider other)
    {
        if (string.IsNullOrEmpty(Filter))
        {
            if (EnterEvent != null)
                EnterEvent.Raise(Value);
            Die();
        }
        else if (Filter == other.tag)
        {
            EnterEvent.Raise(Value);
            Die();
        }
    }



    private void Die()
    {
        if (VFX) Instantiate(VFX, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
