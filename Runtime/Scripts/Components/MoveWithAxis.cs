﻿using UnityEngine;
using UnityEngine.Animations;

public class MoveWithAxis : MonoBehaviour
{
    public Vector3 Axis;
    public Space Space;
    public FloatReference Sensitivity;
    public FloatReference MaxConstraint;
    public StringReference AxisName;
    public BoolReference ShouldConstrain;
    public KeyCode Requirement;

    private float movedAmount;

    private void FixedUpdate()
    {
        if (Requirement != KeyCode.None && !Input.GetKey(Requirement))
            return;

        float amount = Time.deltaTime * Sensitivity * Input.GetAxis(AxisName);

        if (ShouldConstrain.Value)
        {
            if (Mathf.Abs(amount + movedAmount) > MaxConstraint)
            {
                amount = MaxConstraint - Mathf.Abs(movedAmount);
            }
        }

        movedAmount += amount;
        transform.Translate(Axis * amount, Space);
    }
}
