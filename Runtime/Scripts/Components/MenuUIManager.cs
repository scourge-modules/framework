﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUIManager : MonoBehaviour
{
    public GameEvent HideEvent;
    public GameEvent ShowEvent;

    public Canvas Canvas;

    public void OnPlayPressed()
    {
        Canvas.enabled = false;
        HideEvent.Raise();
    }

    private void Awake()
    {
        ShowEvent.Event += HandleReturnToMenu;
    }

    private void OnDestroy()
    {
        ShowEvent.Event -= HandleReturnToMenu;
    }

    private void HandleReturnToMenu()
    {
        Canvas.enabled = true;
    }
}
