﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO camera lerping
public class CameraLooseFollow : MonoBehaviour
{
    public Transform target;
    public float border;

    private Vector3 lastPos;
    public Camera cam;

    private bool isInit = false;

    private void Start()
    {
        isInit = true;
    }

    private void FixedUpdate()
    {
        if (!target) return;

        if (isInit)
        {
            Vector3 vPos = cam.WorldToViewportPoint(target.position);

            if (vPos.x < border || vPos.x > (1 - border) || vPos.y < border || vPos.y > (1 - border))
            {
                transform.position += (target.position - lastPos);
            }
        }

        lastPos = target.position;
    }
}
