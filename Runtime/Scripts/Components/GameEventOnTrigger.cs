﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class GameEventOnTrigger : MonoBehaviour
{
    public StringReference Filter;
    public GameEventReference EnterEvent;
    public GameEventReference ExitEvent;

    private void OnTriggerEnter(Collider other)
    {
        if (string.IsNullOrEmpty(Filter))
        {
            EnterEvent.Raise();
        }
        else if (Filter == other.tag)
        {
            EnterEvent.Raise();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (string.IsNullOrEmpty(Filter))
        {
            if (ExitEvent != null)
                ExitEvent.Raise();
        }
        else if (Filter == other.tag)
        {
            ExitEvent.Raise();
        }
    }
}
