﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressionWidget : MonoBehaviour
{
    public FloatReference CurrentValue;
    public FloatReference BottomValue;
    public FloatReference TopValue;

    public FloatReference LerpSpeed;
    public FloatReference ProgressToGive;

    public BoolReference AlwaysUpdate;

    public Slider fillSlider;

    public GameEventReference OnReachedTopValue;
    public GameEventReference OnReachedBottomValue;

    private void Start()
    {
        UpdateProgressionBar();
    }

    private void Update()
    {
        if (ProgressToGive.Value != 0f || AlwaysUpdate.Value)
        {
            float deltaXP = Mathf.Clamp(LerpSpeed * Time.deltaTime, -ProgressToGive, ProgressToGive);

            ProgressToGive.Value -= deltaXP;
            CurrentValue.Value += deltaXP;
            UpdateProgressionBar();
        }
    }

    private void UpdateProgressionBar()
    {
        fillSlider.minValue = BottomValue;
        fillSlider.maxValue = TopValue;
        fillSlider.value = CurrentValue;

        if (CurrentValue > fillSlider.maxValue)
        {
            if (OnReachedTopValue != null)
                OnReachedTopValue.Raise();
        }
        else if (CurrentValue < fillSlider.minValue)
        {
            if (OnReachedBottomValue != null)
                OnReachedBottomValue.Raise();
        }
    }
}
