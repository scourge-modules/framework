﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableToggle : MonoBehaviour
{
    public BoolReference Variable;
	
	public void OnPressed()
    {
        Variable.Value = !Variable.Value;
    }
}
