﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ImageFadeInOut : MonoBehaviour
{
	public FloatReference InTimer;
	public FloatReference StayTimer;
	public FloatReference OutTimer;

	public Image Image;
	public GameEvent OnFadeOut;

	private IEnumerator Start()
	{
		float timer = 0;
		Color colour = Image.color;
		float totalTime = InTimer + StayTimer + OutTimer;

		while (timer < totalTime)
		{
			if (timer < InTimer)
			{
				colour.a = (float)timer / InTimer;
				Image.color = colour;
			}

			if (timer > InTimer + StayTimer)
			{
				colour.a = (float)(totalTime - timer) / (OutTimer);
				Image.color = colour;
			}

			timer += Time.deltaTime;
			yield return null;
		}

		OnFadeOut.Raise();
	}
}