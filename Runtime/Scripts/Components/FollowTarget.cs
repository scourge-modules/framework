﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public enum FollowType { LATE_UPDATE, FIXED_UPDATE, UPDATE }

    public TransformReference Target;
    private Vector3 initialOffset;
    public BoolReference ShouldLerp;
    public BoolReference ShouldRotate;
    public FloatReference LerpAmount;
    public FollowType followType;

    private void Start()
    {
        initialOffset = transform.position - Target.Value.position;
    }

    private void FixedUpdate() //todo should be a toggle for fixed or update
    {
        if (followType == FollowType.FIXED_UPDATE)
        {
            Perform(Time.fixedDeltaTime);
        }
    }

    private void Update() //todo should be a toggle for fixed or update
    {
        if (followType == FollowType.UPDATE)
        {
            Perform(Time.deltaTime);
        }
    }

    private void LateUpdate()
    {
        if (followType == FollowType.LATE_UPDATE)
        {
            Perform(Time.deltaTime);
        }
    }

    private void Perform(float delta)
    {
        if (ShouldLerp)
        {
            transform.position = Vector3.Lerp(transform.position, Target.Value.position + initialOffset, LerpAmount);
        }
        else
            transform.position = Target.Value.position + initialOffset;

        if (ShouldRotate)
        {
            transform.rotation = Target.Value.rotation;
        }
    }
}
