﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//todo rewrite to include renderer.materials instead of material
public class MaterialToggler : MonoBehaviour
{
    [SerializeField] private ScriptableMaterialList materials;
    [SerializeField] private Renderer mainRenderer;
    [SerializeField] private Renderer[] renderers;

    public void SetIndex(int index)
    {
        index = Mathf.Clamp(index, 0, materials.Count);

        mainRenderer.material = materials[index];
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material = materials[index];
        }
    }
}
