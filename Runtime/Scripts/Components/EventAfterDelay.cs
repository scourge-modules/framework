﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventAfterDelay : MonoBehaviour
{
    [SerializeField] private FloatReference delay;
    [SerializeField] private GameEventReference trigger;

    public void Trigger()
    {
        Invoke("DoTrigger", delay.Value);
    }

    private void DoTrigger()
    {
        trigger.Raise();
    }
}
