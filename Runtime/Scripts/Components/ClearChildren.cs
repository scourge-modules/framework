﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearChildren : MonoBehaviour
{
    public void Destroy()
    {
        transform.DestroyChildren();
    }

    public void Detach()
    {
        transform.DetachChildren();
    }
}
