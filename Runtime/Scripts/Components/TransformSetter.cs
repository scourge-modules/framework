﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformSetter : MonoBehaviour
{
    public Transform TransformToSet; //TODO should be transformref

    private Vector3 initialPosition;
    private Quaternion initialRotation;

    private void Update()
    {
        TransformToSet.position = transform.position;
        TransformToSet.rotation = transform.rotation;
        TransformToSet.localScale = transform.localScale;
    }

    private void Awake()
    {
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }

    private void OnDestroy()
    {
        TransformToSet.SetPositionAndRotation(initialPosition, initialRotation);

    }
}
