﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourToggle : MonoBehaviour
{
    public Behaviour Behaviour;
    public GameEvent EnableEvent;
    public GameEvent DisableEvent;

    private void Awake()
    {
        if (EnableEvent)
            EnableEvent.Event += Enable;
        if (DisableEvent)
            DisableEvent.Event += Disable;
    }

    private void OnDestroy()
    {
        if (EnableEvent)
            EnableEvent.Event -= Enable;
        if (DisableEvent)
            DisableEvent.Event -= Disable;
    }

    public void Toggle()
    {
        Behaviour.enabled = !Behaviour.enabled;
    }

    private void Enable()
    {
        Behaviour.enabled = true;
    }

    private void Disable()
    {
        Behaviour.enabled = false;
    }
}
