﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToggleHelper : MonoBehaviour
{
    public UnityBoolEvent ToggledOn;
    public UnityBoolEvent ToggledOff;
    public void Toggle(bool b)
    {
        Debug.Log(b);
        if (b)
        {
            ToggledOn.Invoke(b);
            // ToggledOff.Invoke(!b);
        }
        else
        {
            // ToggledOn.Invoke(false);
            ToggledOff.Invoke(!b);
        }
    }
}
