﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    public Rigidbody Rigidbody;
    public Rigidbody2D Rigidbody2D;
    
    public BoolReference AffectPosition;
    public BoolReference AffectRotation;

    private Vector3 initialPosition;
    private Quaternion initialRotation;
    private bool IsKinematic;

    public void Initialize()
    {
        initialRotation = transform.rotation;
        initialPosition = transform.position;
        if (Rigidbody) IsKinematic = Rigidbody.isKinematic;
        else if (Rigidbody2D) IsKinematic = Rigidbody2D.isKinematic;
    }

    public void Reset()
    {
        if (AffectPosition) transform.position = initialPosition;
        if (AffectRotation) transform.rotation = initialRotation;
        if (Rigidbody)
        {
            Rigidbody.velocity = Vector3.zero;
            Rigidbody.isKinematic = IsKinematic;
        }
        else if (Rigidbody2D)
        {
            Rigidbody2D.velocity = Vector3.zero;
            Rigidbody2D.isKinematic = IsKinematic;
        }
    }
}
