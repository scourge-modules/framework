﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StringDisplayer : MonoBehaviour // todo we could definitely abstract this instead of adding a new class with everythign for TextMesh for example
{
    [SerializeField] private Text DisplayText;
    [SerializeField] private TextMesh TextMesh;
    [SerializeField] private InputField inputField;
    [SerializeField] private FormattableObject[] FormatVariables;
    [SerializeField] private StringReference String;

    private void Start()
    {
        UpdateText();
    }

    public void SetText(string text)
    {
        String.Value = text;
        UpdateText();
    }

    public void SetFormatVariables(params FormattableObject[] variables)
    {
        FormatVariables = variables;
        UpdateText();
    }

    public void UpdateText()
    {
        if (FormatVariables == null || FormatVariables.Length <= 0)
        {
            if (DisplayText)
            {
                DisplayText.text = String;
            }
            if (TextMesh)
            {
                TextMesh.text = String;
            }
            if (inputField)
            {
                inputField.text = String;
            }
        }
        else
        {
            if (DisplayText)
            {
                DisplayText.text = string.Format(String, FormatVariables);
            }
            if (TextMesh)
            {
                TextMesh.text = string.Format(String, FormatVariables);
            }
            if (inputField)
            {
                inputField.text = string.Format(String, FormatVariables);
            }
        }
    }
}
