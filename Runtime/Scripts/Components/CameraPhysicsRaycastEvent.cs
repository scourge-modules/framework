﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraPhysicsRaycastEvent : MonoBehaviour
{
	[System.Serializable]
	public class Vector3Event : UnityEvent<Vector3>
	{

	}
	public Vector3Event OnNewHit;

	public void Trigger()
	{
		Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit))
		{
			OnNewHit.Invoke(hit.point);
		}
	}
}
