﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class GameEventOnCollision : MonoBehaviour
{
    public StringReference Filter;
    public GameEvent CollideEvent;

    private void OnCollisionEnter(Collision other)
    {
        if (string.IsNullOrEmpty(Filter))
        {
            CollideEvent.Raise();
        }
        else if (Filter == other.gameObject.tag)
        {
            CollideEvent.Raise();
        }
    }
}
