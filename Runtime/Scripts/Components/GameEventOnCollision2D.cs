﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class GameEventOnCollision2D : MonoBehaviour
{
    public StringReference Filter;
    public GameEvent CollideEvent;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (string.IsNullOrEmpty(Filter))
        {
            CollideEvent.Raise();
        }
        else if (Filter == other.gameObject.tag)
        {
            CollideEvent.Raise();
        }
    }
}
