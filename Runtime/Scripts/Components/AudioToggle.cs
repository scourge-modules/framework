﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO this can maybe just be a general toggle? leaving in core for now
public class AudioToggle : MonoBehaviour
{
    public BoolReference IsAudioActive;
    public GameObject AudioOffObject;
    public GameObject AudioOnObject;

    private void Awake()
    {
        UpdateImages();
    }

    public void OnPressed()
    {
        IsAudioActive.Value = !IsAudioActive.Value;

        UpdateImages();
    }

    private void UpdateImages()
    {
        if (AudioOnObject)
            AudioOnObject.SetActive(IsAudioActive);

        if (AudioOffObject)
            AudioOffObject.SetActive(!IsAudioActive);

        AudioListener.volume = IsAudioActive ? 1f : 0f; ; //TODO
    }
}
