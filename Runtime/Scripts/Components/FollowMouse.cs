﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour 
{
    public FloatReference playableAreaBuffer;
    public CoordinateType Coordinates;
    private Vector3 lastPos;
    private Vector3 delta;

    private Vector3 initPos;

    private bool canMove = true;
    private bool detectedMouseDown = false;

    private void Awake()
    {
        initPos = transform.localPosition;
    }

	private void Update () 
    {
        CheckInput();	
	}

    private void CheckInput()
    {
        if (!canMove) return;

        if (Input.GetMouseButtonDown(0))
        {
            detectedMouseDown = true;
            lastPos = Coords.GetMousePosition(Coordinates);
        }
        else if (Input.GetMouseButton(0) && detectedMouseDown)
        {
            delta = Coords.GetMousePosition(Coordinates) - lastPos;

            Move();

            lastPos = Coords.GetMousePosition(Coordinates);
        }
    }

    private void Move()
    {
        transform.position = Vector3.Lerp(transform.position, Coords.GetMousePosition(Coordinates), 0.5f);

        float x = transform.position.x;
        float y = transform.position.y;

        if (Coordinates == CoordinateType.SCREEN)
        {
            if (transform.position.x < playableAreaBuffer) x = playableAreaBuffer;
            if (transform.position.x > Screen.width - playableAreaBuffer) x = Screen.width - playableAreaBuffer;
            if (transform.position.y < playableAreaBuffer) y = playableAreaBuffer;
            if (transform.position.y > Screen.height - playableAreaBuffer) y = Screen.height - playableAreaBuffer;
        }

        transform.position = new Vector3(x, y, 0f);
    }
}
