﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public GameEvent Trigger;

    public SceneField[] ScenesToLoad;
    public SceneField[] ScenesToUnload;

    private void Awake()
    {
        if (Trigger)
        {
            Trigger.Event += Load;
            Trigger.Event += Unload;
        }
    }

    private void OnDestroy()
    {
        if (Trigger)
        {
            Trigger.Event -= Load;
            Trigger.Event -= Unload;
        }
    }

    public void Unload()
    {
        if (ScenesToUnload != null)
        {
            for (int i = 0; i < ScenesToUnload.Length; i++)
            {
                ScenesToUnload[i].Unload();
            }
        }
    }

    public void Load()
    {
        if (ScenesToLoad != null)
        {
            for (int i = 0; i < ScenesToLoad.Length; i++)
            {
                ScenesToLoad[i].Load();
            }
        }
    }

    public void LoadSingle()
    {
        ScenesToLoad[0].Load(LoadSceneMode.Single);
    }
}
