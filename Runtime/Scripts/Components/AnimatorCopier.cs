﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorCopier : MonoBehaviour
{
    [SerializeField] private AnimatorReference destinationAnimator;
    [SerializeField] private AnimatorReference sourceAnimator;

    public void SetTargetAnimator(Animator targetAnim)
    {
        sourceAnimator.Value = targetAnim;
        destinationAnimator.Value.runtimeAnimatorController = sourceAnimator.Value.runtimeAnimatorController;
    }

    private void Update()
    {
        if (sourceAnimator.Value)
        {
            foreach (AnimatorControllerParameter parameter in sourceAnimator.Value.parameters)
            {
                if (parameter.type == AnimatorControllerParameterType.Bool)
                    destinationAnimator.Value.SetBool(parameter.name, sourceAnimator.Value.GetBool(parameter.name));
                if (parameter.type == AnimatorControllerParameterType.Float)
                    destinationAnimator.Value.SetFloat(parameter.name, sourceAnimator.Value.GetFloat(parameter.name));
                if (parameter.type == AnimatorControllerParameterType.Int)
                    destinationAnimator.Value.SetInteger(parameter.name, sourceAnimator.Value.GetInteger(parameter.name));
                if (parameter.type == AnimatorControllerParameterType.Trigger)
                {
                    bool isOn = sourceAnimator.Value.GetBool(parameter.name);
                    bool ourIsOn = destinationAnimator.Value.GetBool(parameter.name);
                    if (isOn != ourIsOn)
                    {
                        destinationAnimator.Value.SetTrigger(parameter.name);
                    }
                }
            }
        }
    }
}
