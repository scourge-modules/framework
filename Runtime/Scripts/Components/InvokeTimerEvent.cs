﻿using UnityEngine;
using UnityEngine.Events;

public class InvokeTimerEvent : MonoBehaviour
{
    public FloatReference Delay;
    public FloatReference Repeat;
    public GameEventReference OnTrigger;

    private void Start()
    {
        if (Repeat.Value > 0f)
        {
            InvokeRepeating("Trigger", Delay, Repeat);
        }
        else
        {
            Invoke("Trigger", Delay);

        }
    }

    public void Trigger()
    {
        if (OnTrigger != null)
            OnTrigger.Raise();
    }
}
