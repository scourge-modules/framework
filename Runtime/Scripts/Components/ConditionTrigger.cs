﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConditionTrigger : MonoBehaviour
{
    public ConditionReference Condition;

    public UnityEvent TriggerdEvent;
    public UnityEvent FailedTriggerEvent;

    public void TryTrigger()
    {
        if (Condition.Value.IsMet())
        {
            TriggerdEvent.Invoke();
        }
        else
        {
            FailedTriggerEvent.Invoke();
        }
    }

}
