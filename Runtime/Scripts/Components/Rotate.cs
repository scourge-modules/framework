﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
	public Vector3 angularVelocity;
    public Space Space;

    private void FixedUpdate ()
	{
		transform.Rotate(angularVelocity * Time.deltaTime, Space );
	}
}
