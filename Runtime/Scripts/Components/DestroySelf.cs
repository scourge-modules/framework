﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelf : MonoBehaviour
{
    [SerializeField] private GameObject VFXOnDeath;

    public void Die()
    {
        if (VFXOnDeath != null)
        {
            Instantiate(VFXOnDeath, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
