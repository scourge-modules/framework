﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CoroutineHelper : MonoBehaviour
{
    public static CoroutineHelper instance;

    private void OnEnable()
    {
        instance = this;
    }

    public void RunCoroutine(IEnumerator routine)
    {
        StartCoroutine(routine);
    }
}
