﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMeshDisplayer : MonoBehaviour {

    public TextMesh DisplayText;
    public FormattableObject[] FormatVariables;
    public StringReference String;

    private void Start()
    {
        UpdateText();
    }

    public void SetText(string text)
    {
        String.Value = text;
        UpdateText();
    }

    public void SetFormatVariables(params FormattableObject[] variables)
    {
        FormatVariables = variables;
        UpdateText();
    }

    public void UpdateText()
    {
        if (FormatVariables == null || FormatVariables.Length <= 0)
        {
            DisplayText.text = String;
        }
        else
        {
            DisplayText.text = string.Format(String, FormatVariables);
        }
    }
}
