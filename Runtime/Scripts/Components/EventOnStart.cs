﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventOnStart : MonoBehaviour
{
    public GameEventReference Event;

    private void Start()
    {
        Event.Raise();
    }
}
