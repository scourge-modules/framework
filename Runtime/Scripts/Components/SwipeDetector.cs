﻿using UnityEngine;
using UnityEngine.Events;

public class SwipeDetector : MonoBehaviour
{
    public FloatReference MaxAngleDiff;
    public FloatReference MinDist;
    public FloatReference MinSpeed;

    public GameEvent SwipeUpEvent;
    public GameEvent SwipeDownEvent;
    public GameEvent SwipeLeftEvent;
    public GameEvent SwipeRightEvent;

    private Vector2 m_startPos = Vector2.zero;
    private float m_mouseDownTime = 0f;
    private bool m_wasInitialized = false;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_startPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            m_mouseDownTime = Time.time;
            m_wasInitialized = true;
        }
        else if (Input.GetMouseButtonUp(0) && m_wasInitialized)
        {
            Vector2 swipe = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - m_startPos;

            float timeDiff = Time.time - m_mouseDownTime;
            float speed = swipe.magnitude / timeDiff;

            if (speed > MinSpeed && swipe.magnitude > MinDist)
            {
                swipe.Normalize();

                float angle = Mathf.Acos(Vector2.Dot(swipe, Vector2.left)) * Mathf.Rad2Deg;

                if (angle < MaxAngleDiff)
                {
                    SwipeRightEvent.Raise();
                }
                else if ((180.0f - angle) < MaxAngleDiff)
                {
                    SwipeLeftEvent.Raise();
                }
                else
                {
                    angle = Mathf.Acos(Vector2.Dot(swipe, Vector2.up)) * Mathf.Rad2Deg;
                    if (angle < MaxAngleDiff)
                    {
                        SwipeUpEvent.Raise();
                    }
                    else if ((180.0f - angle) < MaxAngleDiff)
                    {
                        SwipeDownEvent.Raise();
                    }
                }
            }
        }
    }
}