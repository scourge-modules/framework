﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Framework
{
    public class Spawner : MonoBehaviour
    {
        public GameObject single;
        public ScriptableGameObjectList ObjectsToSpawn; //TODO should be a reference
        public GameEvent SpawnEvent;
        public bool IsParent = false;
        public bool isVolume = false;

        private void Awake()
        {
            if (SpawnEvent)
                SpawnEvent.Event += SpawnAllInVolume;
        }

        private void OnDestroy()
        {
            if (SpawnEvent)
                SpawnEvent.Event -= SpawnAllInVolume;
        }

        public void SpawnAllInVolume()
        {
            if (ObjectsToSpawn != null)
            {
                for (int i = 0; i < ObjectsToSpawn.Count; i++)
                {
                    Spawn(i);
                }
            }
            else
            {
                SpawnSingle();
            }
        }

        public void SpawnRandom()
        {
            Spawn(Random.Range(0, ObjectsToSpawn.Count));
        }

        private void Spawn(int i)
        {
            GameObject go = Instantiate(ObjectsToSpawn[i], GetPosition(), Quaternion.identity);
            if (IsParent)
            {
                go.transform.SetParent(transform);
            }
        }


        public void SpawnSingle()
        {
            if (IsParent)
                Instantiate(single, transform);//TODO
            else
                Instantiate(single, transform.position, Quaternion.identity);
        }


        private Vector3 GetPosition()
        {
            if (!isVolume) return transform.position;

            float x = Random.Range(-transform.localScale.x / 2f, transform.localScale.x / 2f);
            float y = Random.Range(-transform.localScale.y / 2f, transform.localScale.y / 2f);
            float z = Random.Range(-transform.localScale.z / 2f, transform.localScale.z / 2f);

            return transform.position + new Vector3(x, y, z);
        }
    }
}