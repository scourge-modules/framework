﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Static/System/DebugSystem")]
public class DebugManager : ScriptableSingleton<DebugManager>
{
    public static void L(object obj, string color = "red", params object[] other)
    {
        D.L(obj, color, other);
    }

    public void Log(string message)
    {
        D.LR(message);
    }
}
