﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

//TODO this seems like it could just be a stateSwitch/use its component

public class KeyCodeListener : MonoBehaviour
{
    [System.Serializable]
    public class KeyCodeEvent
    {
        public KeyCode key;
        public GameEventReference OnDown;
        public GameEventReference OnUp;

        public void Check()
        {
            if (Input.GetKeyDown(key))
            {
                if (OnDown != null)
                {
                    OnDown.Raise();
                }
            }
            else if (Input.GetKeyUp(key))
            {
                if (OnUp != null)
                {
                    OnUp.Raise();
                }
            }
        }
    }

    public List<KeyCodeEvent> Events;

    private void Update()
    {
        for (int i = 0; i < Events.Count; i++)
        {
            Events[i].Check();
        }
    }
}
