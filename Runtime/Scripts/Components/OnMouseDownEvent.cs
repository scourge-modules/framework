﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseDownEvent : MonoBehaviour
{
    [SerializeField] private GameEventReference onMouseDown;

    private void OnMouseDown()
    {
        if (onMouseDown!=null)
        {
            onMouseDown.Raise();
        }
    }
}
