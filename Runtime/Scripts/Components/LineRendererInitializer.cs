﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

//todo this should definitely be split up
public class LineRendererInitializer : MonoBehaviour
{
    [SerializeField] private ScriptableVectorList test;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private FloatReference takeEveryNthValue;
    [SerializeField] private GameEventReference OnPointsUpdated;

    private void Start()
    {
        UpdatePoints();
    }

    [ContextMenu("Update")]
    public void UpdatePoints()
    {
        lineRenderer.positionCount = test.Count;
        if (takeEveryNthValue.Value == 0)
        {
            lineRenderer.SetPositions(test.ToArray());
        }
        else
        {
            lineRenderer.SetPositions(test.Where((x, i) => i % takeEveryNthValue == 0).ToArray());
        }
        OnPointsUpdated.Raise();
    }
}

