﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConditionalTrigger : MonoBehaviour
{
    public BoolReference Condition;
    public BoolReference IsInverse;

    public UnityEvent TriggerdEvent;
    public UnityEvent FailedTriggerEvent;

    public void TryTrigger()
    {
        if (Condition.Value != IsInverse.Value)
        {
            TriggerdEvent.Invoke();
        }
        else
        {
            FailedTriggerEvent.Invoke();
        }
    }

}
