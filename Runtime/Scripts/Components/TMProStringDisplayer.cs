using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TMProStringDisplayer : MonoBehaviour // todo we could definitely abstract this instead of adding a new class with everythign for TextMesh for example
{
	[SerializeField] protected TextMeshProUGUI tmpTextGUI;
	[SerializeField] protected TextMeshPro tmpText;
	[SerializeField] protected FormattableObjectReference[] FormatVariables;
	[SerializeField] protected StringReference String;
	[SerializeField] protected BoolReference shouldUpdate;

	public string Text => String.Value;

	protected virtual void Update()
	{
		if (shouldUpdate.Value)
		{
			UpdateText();
		}
	}

	protected virtual void Start()
	{
		UpdateText();
	}

	protected void SetText(string text)
	{
		String.Value = text;
		UpdateText();
	}

	protected void SetFormatVariables(params FormattableObjectReference[] variables)
	{
		FormatVariables = variables;
		UpdateText();
	}

	protected virtual void UpdateText()
	{
		if (FormatVariables == null || FormatVariables.Length <= 0)
		{
			if (tmpTextGUI)
			{
				tmpTextGUI.text = String;
			}
			if (tmpText)
			{
				tmpText.text = String;
			}
		}
		else
		{
			if (tmpText)
			{
				tmpText.text = string.Format(String, FormatVariables);
			}
			if (tmpTextGUI)
			{
				tmpTextGUI.text = string.Format(String, FormatVariables);
			}
		}
	}
}
