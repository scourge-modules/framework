﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//TODO this needs some rigorous testing, articularly the update loop
// Special care needs to be taken for starting in the Negative state and then calling on negative
// should be a component that has a reference to scriptable state switch or local. 
// if we want 1 key to be a toggle - keycode listener currently wont help

public class StateSwitcher : MonoBehaviour
{
    public enum State { ONPOSITIVE, POSITIVE, ONNEGATIVE, NEGATIVE }

    [SerializeField] private ToggleEvent toggleEvent;
    [SerializeField] private GameEvent SetStatePositiveEvent; //If Inventory was on and you wanted to show all inventory widgets, you couldnt hook up the toggle component here
    [SerializeField] private GameEvent SetStateNegativeEvent; //Can be "Hide all UI event"
    [SerializeField] private State state;
    [SerializeField] private GameEventReference OnPositive;
    [SerializeField] private GameEventReference OnNegative;
    [SerializeField] private BoolReference cap01;

    private int count = 1;

    private bool setHolding = false;

    private void Awake()
    {
        if (SetStateNegativeEvent)
            SetStateNegativeEvent.Event += SetStateNegative;
        if (SetStatePositiveEvent)
            SetStatePositiveEvent.Event += SetStatePositive;
        if (toggleEvent)
            toggleEvent.Event += Toggle;


        count = state == State.POSITIVE ? 1 : 0;
    }

    private void OnDestroy()
    {
        if (SetStateNegativeEvent)
            SetStateNegativeEvent.Event -= SetStateNegative;
        if (SetStatePositiveEvent)
            SetStatePositiveEvent.Event -= SetStatePositive;
        if (toggleEvent)
            toggleEvent.Event -= Toggle;
    }

    private void Start()
    {
        if (state == State.ONNEGATIVE)
        {
            SetStateNegative();
        }

        if (state == State.ONPOSITIVE)
        {
            SetStatePositive();
        }
    }

    [ContextMenu("Set Negative")]
    public void SetStateNegative()
    {
        count--;
        if (cap01 && count < 0)
            count = 0;

        if (count == 0)
        {
            OnNegative.Raise();
            state = State.ONNEGATIVE;
        }
    }

    [ContextMenu("SetPositive")]
    public void SetStatePositive()
    {
        count++;
        if (cap01 && count > 1)
            count = 1;

        if (count == 1)
        {
            OnPositive.Raise();
            state = State.ONPOSITIVE;
        }
    }

    [ContextMenu("Toggle")]
    public void Toggle()
    {
        if (state == State.ONPOSITIVE || state == State.POSITIVE)
        {
            SetStateNegative();
        }
        else
        {
            SetStatePositive();
        }
    }

    private void Update() //TODO not sure if this is necessary. Seems more likely that another component stopstarts here
    {
        if (setHolding)
        {
            if (state == State.ONNEGATIVE)
                state = State.NEGATIVE;
            else if (state == State.ONPOSITIVE)
                state = State.POSITIVE;
            setHolding = false;
        }

        if (state == State.ONNEGATIVE || state == State.ONPOSITIVE)
        {
            setHolding = true;
        }
    }

    public bool IsPositive() //Notsure if necessary
    {
        return state == State.POSITIVE;
    }

    public bool IsNegative()
    {
        return state == State.NEGATIVE;
    }
}
