﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class Pickup2D : MonoBehaviour
{
    public StringReference Filter;
    public IntEvent EnterEvent;
    public GameObject VFX;
    public IntReference Value;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (string.IsNullOrEmpty(Filter))
        {
            EnterEvent.Raise(Value);
            Die();
        }
        else if (Filter == other.tag)
        {
            EnterEvent.Raise(Value);
            Die();
        }
    }


    
    private void Die()
    {
        if (VFX) Instantiate(VFX, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
