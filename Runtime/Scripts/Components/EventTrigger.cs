﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTrigger : MonoBehaviour
{
    [SerializeField] private GameEvent trigger;
    [SerializeField] private GameEventReference Response;

    private void Awake()
    {
        if (trigger)
        {
            trigger.Event += Trigger;
        }
    }
    private void OnDestroy()
    {
        if (trigger)
        {
            trigger.Event -= Trigger;
        }
    }

    [ContextMenu("Manual Trigger")]
    public void Trigger()
    {
        Response.Raise();
    }
}


