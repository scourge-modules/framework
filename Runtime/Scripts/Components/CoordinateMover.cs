﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoordinateMover : MonoBehaviour
{
    public CoordinateType Coordinates;
	
	private void Start ()
    {
        //transform.position = transform.position.Convert(Coordinates);
        transform.localScale = transform.localScale.Convert(Coordinates);
	}
}
