﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisListener : MonoBehaviour
{
    [System.Serializable]
    public class AxisEvent
    {
        public AxisReference axis;
        public GameEventReference OnDown;
        public GameEventReference OnUp;
        public FloatEventReference Changed;

        public void Check()
        {
            if (axis.Down)
            {
                if (OnDown != null)
                {
                    OnDown.Raise();
                }
            }
            else if (axis.Up)
            {
                if (OnUp != null)
                {
                    OnUp.Raise();
                }
            }

            if (Changed != null && axis.Axis != 0f)
            {
                Changed.Raise(axis.Axis);
            }
        }
    }

    public List<AxisEvent> Events = new List<AxisEvent>();

    private void Update()
    {
        for (int i = 0; i < Events.Count; i++)
        {
            Events[i].Check();
        }
    }
}
