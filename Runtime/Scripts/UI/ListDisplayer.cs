﻿using UnityEngine;
using UnityEngine.UI;

public class ListDisplayer : MonoBehaviour
{
    public IndexData CellPrefab;
    public Transform Container;
    public ScriptableIList List; // should be a list reference
    public bool PrefabIsInstance;

    public bool populateOnStart = true;

    private void Start()
    {
        if (populateOnStart)
        {
            Populate();
        }
    }

    public void Populate()
    {
        for (int i = 0; i < List.Count; i++)
        {
            IndexData cell;

            if (i == 0 && PrefabIsInstance)
                cell = CellPrefab;
            else
                cell = Instantiate(CellPrefab, Container);

            cell.Configure(i);
        }
    }
}