﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexData : MonoBehaviour
{
    public IntReference Index;
    [SerializeField] private GameEventReference onConfigure;
    [SerializeField] private BoolReference isConfigured;

    public bool IsConfigured => isConfigured.Value;

    public void Configure(int index)
    {
        Index.Value = index;
        isConfigured.Value = true;
        onConfigure.Raise();
    }
}
