﻿using System.Linq;

public class Tree
{
    public Branch root;

    public Tree(params string[] branchNames)
    {
        branchNames = branchNames.Select(x => x.Trim()).Where(x => !string.IsNullOrEmpty(x)).Distinct().ToArray();
        root = new Branch(null, "Root", -1, branchNames);
    }

    public override string ToString()
    {
        return root.ToString();
    }
}
