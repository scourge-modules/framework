﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

//this is aimed to have the whole structure built once and upon amendment will have to fully reconstruct

public class Branch
{
    protected Dictionary<string, Branch> branches = new Dictionary<string, Branch>();
    protected string name;
    protected Branch parent;
    protected int indentLevel = 0;
    protected List<string> bases = new List<string>();

    public string Name => name;
    public string FullName => parent == null ? name : parent.FullName + "/" + name;
    public Branch Parent => parent;
    public Dictionary<string, Branch> Branches => branches;

    public int IndentLevel
    {
        get => indentLevel;
        set => indentLevel = value;
    }

    public Branch(Branch parent, string name, int indexLevel, params string[] branchNames)
    {
        this.name = name;
        this.parent = parent;
        indentLevel = indexLevel;

        BuildBranchDictionary(branchNames);
    }

    private void BuildBranchDictionary(string[] branchNames)
    {
        branches.Clear();
        bases.Clear();

        for (int i = 0; i < branchNames.Length; i++)
        {
            string[] subNames = branchNames[i].Split('/');
            if (subNames.Length > 0)
            {
                bases.Add(subNames[0]);
            }
        }

        bases = bases.Distinct().ToList();

        for (int i = 0; i < bases.Count; i++)
        {
            string[] names = branchNames.Where(x => x.StartsWith(bases[i]) && bases[i].Trim() != x.Trim() && !string.IsNullOrEmpty(x)).ToArray();
            names = names.Select(x => x.Substring(bases[i].Length + 1).Trim()).ToArray();

            if (!string.IsNullOrWhiteSpace(bases[i])) //figure out why this is needed here and not caught in the line above
            {
                Branch b = new Branch(this, bases[i], indentLevel + 1, names);
                branches.Add(bases[i], b);
            }
        }
    }

    public override string ToString()
    {
        Debug.LogError(indentLevel);
        string indents = new System.String('\t', indentLevel);
        string result = indents + name + "\n";

        foreach (KeyValuePair<string, Branch> branch in branches)
        {
            result += branches[branch.Key].ToString();

        }
        return result;
    }
}
