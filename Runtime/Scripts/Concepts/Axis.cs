﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Scourge
{
    [CreateAssetMenu(menuName = "New Static/Manager/Axis")]
    public class Axis : ScriptableObject
    {
        public List<ScriptableAxis> all = new List<ScriptableAxis>();

        public static Axis Instance;

        protected virtual void OnEnable()
        {
            Instance = this;
        }

        // [SerializeField] private string path = string.Empty;

#if UNITY_EDITOR
    [ContextMenu("Clean")]
    public void Clean()
    {
        for (int i = 0; i < all.Count; i++)
        {
            DestroyImmediate(all[i], true);
        }

        all.Clear();
        Object inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];

        SerializedObject obj = new SerializedObject(inputManager);

        SerializedProperty axisArray = obj.FindProperty("m_Axes");

        string path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(this));

        List<string> uniqueNames = new List<string>();

        for (int i = 0; i < axisArray.arraySize; ++i)
        {
            SerializedProperty axis = axisArray.GetArrayElementAtIndex(i);
            string name = axis.FindPropertyRelative("m_Name").stringValue;
            uniqueNames.Add(name);
        }

        uniqueNames = uniqueNames.Distinct().ToList();

        for (int i = 0; i < uniqueNames.Count; ++i)
        {
            ScriptableAxis asset = ScriptableObject.CreateInstance<ScriptableAxis>();
            asset.Value = asset.name = uniqueNames[i];
            asset.PersistData = true;

            all.Add(asset);

            AssetDatabase.AddObjectToAsset(asset, AssetDatabase.GetAssetPath(this));
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        EditorUtility.FocusProjectWindow();
    }

    //     public void OnAfterDeserialize()
    //     {
    //         // DoCheckOnIm();
    //     }

    //     protected void OnEnable()
    //     {
    //         DoCheckOnIm();
    //         Check();
    //     }
    //     // protected override void OnEnable()
    //     // {
    //     //     base.OnEnable();
    //     //     path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(this));
    //     // }

    //     public void OnBeforeSerialize()
    //     {
    //         Check();

    //     }

    //     private void Check()
    //     {
    //         string hmm = AssetDatabase.GetAssetPath(this);
    //         if (string.IsNullOrWhiteSpace(hmm))
    //         {
    //             return;
    //         }
    //         path = Path.GetDirectoryName(hmm);
    //         if (path != string.Empty)
    //         {
    //             im = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];

    //             DoCheckOnIm();

    //         }

    //     }

    //     private void DoCheckOnIm()
    //     {
    //         axisArray = new SerializedObject(im).FindProperty("m_Axes");
    //         if (axisArray != null)
    //         {

    //             for (int i = 0; i < axisArray.arraySize; ++i)
    //             {
    //                 SerializedProperty axis = axisArray.GetArrayElementAtIndex(i);
    //                 string name = axis.FindPropertyRelative("m_Name").stringValue;

    //                 if (all != null && name != null && all.Count != axisArray.arraySize)
    //                 {
    //                     Clean();
    //                 }
    //                 else if (name != all[i].Value)
    //                 {
    //                     Clean();

    //                 }
    //             }
    //         }
    //     }
#endif

    }
}