﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class ScriptableObjectBase : ScriptableObject
{
#if UNITY_EDITOR
    public bool PersistData;
#endif

    protected virtual void OnDisable()
	{
#if UNITY_EDITOR
        UnityEditor.EditorApplication.playModeStateChanged -= HandlePlayModeChanged;
#endif
	}

	protected virtual void OnEnable()
	{
        hideFlags = HideFlags.DontUnloadUnusedAsset;
#if UNITY_EDITOR
        UnityEditor.EditorApplication.playModeStateChanged += HandlePlayModeChanged;
#endif
	}

#if UNITY_EDITOR
	protected virtual void HandlePlayModeChanged(PlayModeStateChange change) //TODO this should really go into an overrideable function that classes can inherit from without using unityeditor
	{
        if (!PersistData)
        {
            try
            {
                Resources.UnloadAsset(this);
            }
            catch { }
        }
	}
#endif
}
