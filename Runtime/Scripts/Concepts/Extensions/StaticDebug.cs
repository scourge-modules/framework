using UnityEngine;

public static class D
{
    public static void L(this object obj, string color = "red", params object[] objects)
    {
        string extraData = " (";

        for (int i = 0; i < objects.Length; i++)
        {
            extraData += objects[i];
            extraData += (i + 1 < objects.Length) ? ", " : ")";
        }

        Debug.Log("<color=" + color + ">" + obj + "</color>" + extraData);
    }

    public static void LR(this object obj, params object[] objects) { D.L(obj, "red", objects); }
    public static void LG(this object obj, params object[] objects) { D.L(obj, "green", objects); }
    public static void LW(this object obj, params object[] objects) { D.L(obj, "white", objects); }
    public static void LB(this object obj, params object[] objects) { D.L(obj, "blue", objects); }
    public static void LY(this object obj, params object[] objects) { D.L(obj, "yellow", objects); }
    public static void LO(this object obj, params object[] objects) { D.L(obj, "orange", objects); }
}