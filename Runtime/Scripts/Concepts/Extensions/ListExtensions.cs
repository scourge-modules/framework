﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    public static T GetRandom<T>(this IList<T> list)
    {
        int randomIndex = Random.Range(0, list.Count);
        return list[randomIndex];
    }

    public static T Last<T>(this IList<T> list)
    {
        return list[list.Count - 1];
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        int p = list.Count;
        for (int n = p - 1; n > 0; n--)
        {
            int r = Random.Range(0, n);
            T t = list[r];
            list[r] = list[n];
            list[n] = t;
        }
    }

    //Combine one list of items with another
    public static IList<T> Concatenate<T>(this IList<T> list, IList<T> listToAdd)
    {
        if (list != null && listToAdd != null)
        {
            for (int i = 0; i < listToAdd.Count; i++)
            {
                list.Add(listToAdd[i]);
            }
        }
        return list;
    }

    //Replace every item in the list with the supplied item
    public static void Fill<T>(this List<T> list, T itemToFill)
    {
        if (list != null)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = itemToFill;
            }
        }
    }

    //Replace the item range in the list with the supplied item
    public static void Fill<T>(this IList<T> list, T itemToFill, int start, int end)
    {
        if (list != null)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (i >= start && i <= end && start <= list.Count && end <= list.Count)
                {
                    list[i] = itemToFill;
                }
            }
        }
    }

    public static int Push<T>(this IList<T> list, T itemToPush)
    {
        if (list != null)
        {
            list.Add(itemToPush);
            return list.Count;
        }
        return 0;
    }

    public static T Pop<T>(this List<T> list)
    {
        if (list != null)
        {
            T item;
            item = list.Last();
            list.Remove(item);
            return item;
        }
        return default(T);
    }

    public static List<T> Split<T>(this IList<T> list, int start, int end)
    {
        List<T> newList = new List<T>();

        if (list != null)
        {
            if (start <= list.Count && end <= list.Count)
            {
                for (int i = start; i < end; i++)
                {
                    newList.Add(list[i]);
                }
            }
            else
            {
                Debug.LogError("Start or end out of range of list.");
            }
            return newList;
        }
        return newList;
    }

    public static void DestroyAllAndClear(this IList<Component> list)
    {
        for (int i = list.Count - 1; i >= 0; i--)
        {
            if (list[i] != null)
            {
                GameObject.Destroy(list[i]);
            }
        }
        list.Clear();
    }

    public static void DestroyAllAndClearGO(this IList<Component> list)
    {
        for (int i = list.Count - 1; i >= 0; i--)
        {
            if (list[i] != null)
            {
                GameObject.Destroy(list[i].gameObject);
            }
        }
        list.Clear();
    }

    public static void DestroyAllAndClear(this IList<GameObject> list)
    {
        for (int i = list.Count - 1; i >= 0; i--)
        {
            if (list[i] != null)
            {
                GameObject.Destroy(list[i]);
            }
        }
        list.Clear();
    }
}