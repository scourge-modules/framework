﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class MonoBehaviourExtensions
{
    public static bool IsPrefab(this MonoBehaviour monoBehaviour)
    {
        return string.IsNullOrEmpty(monoBehaviour.gameObject.scene.name);
    }

    public static bool IsPrefab(this GameObject gameObject)
    {
        return string.IsNullOrEmpty(gameObject.scene.name);
    }

    public static void InvokeSafe(this MonoBehaviour monoBehaviour, Action action, float delay = 0f)
    {
        monoBehaviour.Invoke(action.Method.Name, delay);
    }

    public static void InvokeRepeatingSafe(this MonoBehaviour monoBehaviour, Action action, float delay = 0f, float repeat = 1f)
    {
        monoBehaviour.InvokeRepeating(action.Method.Name, delay, repeat);
    }

    public static void DestroyChildren(this Transform trans)
    {
        if (Application.isPlaying)
        {
            for (int i = trans.childCount - 1; i >= 0; i--)
            {
                MonoBehaviour.Destroy(trans.GetChild(i).gameObject);
            }
        }
        else
        {
            for (int i = trans.childCount - 1; i >= 0; i--)
            {
                MonoBehaviour.DestroyImmediate(trans.GetChild(i).gameObject);
            }
        }
    }
}
