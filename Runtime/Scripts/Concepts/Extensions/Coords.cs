﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Coords
{
    public static Vector3 GetMousePosition(CoordinateType type, float zPos = 0f, Camera cam = null)
    {
        Vector3 position = Input.mousePosition;

        if (type == CoordinateType.WORLD)
        {
            if (cam == null)
                cam = Camera.main;
            position.z = zPos;
            return cam.ScreenToWorldPoint(position);
        }
        else
            return position;
    }

    public static Vector3 Convert(this Vector3 position, CoordinateType type, Camera cam = null)
    {
        if (cam == null)
            cam = Camera.main;
        if (type == CoordinateType.WORLD)
            return cam.ScreenToWorldPoint(position);
        else
            return RectTransformUtility.WorldToScreenPoint(cam, position);
    }
}


public enum CoordinateType
{
    WORLD,
    SCREEN
}