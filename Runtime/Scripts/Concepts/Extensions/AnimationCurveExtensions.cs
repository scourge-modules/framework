﻿using System.Linq;
using UnityEngine;

public static class AnimationCurveExtensions
{
    public static float Duration(this AnimationCurve curve)
    {
        return curve.keys[curve.keys.Length - 1].time;
    }

    public static float Evaluate01(this AnimationCurve curve, float t)
    {
        return curve.Evaluate(t) / curve.Duration();
    }

    public static void Normalize(this AnimationCurve curve) //This isnt working
    {
        for (int i = 0; i < curve.keys.Length; i++)
        {
            curve.Duration().L("red", curve.keys[i].time, i);
            curve.keys[i].time /= curve.Duration();
        }
    }
}
