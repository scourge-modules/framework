﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScriptableGameObject : ScriptableObject
{
    [SerializeField] private ScriptableGameObjectHelper helper;

    private void Initialize()
    {
        if (helper != null && Application.isPlaying)
        {
            helper = new GameObject(name).AddComponent<ScriptableGameObjectHelper>();
            helper.Initialize(Awake, OnEnable, OnDisable, OnDestroy, Update, LateUpdate, FixedUpdate);
        }
    }

    protected virtual void Awake()
    {
        Initialize();
    }

    protected virtual void OnEnable()
    {
        Initialize();
    }

    protected virtual void OnDisable()
    {
        Initialize();
    }

    protected virtual void OnDestroy()
    {
        Initialize();
    }

    protected virtual void Update()
    {
        Initialize();
    }

    protected virtual void LateUpdate()
    {
        Initialize();
    }

    protected virtual void FixedUpdate()
    {
        Initialize();
    }
}
