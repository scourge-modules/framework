﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableGameObjectHelper : MonoBehaviour
{
    private Action onAwake;
    private Action onOnEnable;
    private Action onOnDisable;
    private Action onOnDestroy;
    private Action onUpdate;
    private Action onLateUpdate;
    private Action onFixedUpdate;

    public void Initialize(Action onAwake, Action onOnEnable, Action onOnDisable, Action onOnDestroy, Action onUpdate, Action onLateUpdate, Action onFixedUpdate)
    {
        this.onAwake = onAwake;
        this.onOnEnable = onOnEnable;
        this.onOnDisable = onOnDisable;
        this.onOnDestroy = onOnDestroy;
        this.onUpdate = onUpdate;
        this.onLateUpdate = onLateUpdate;
        this.onFixedUpdate = onFixedUpdate;
    }

    private void Awake()
    {
        if (onAwake != null)
        {
            onAwake.Invoke();
        }
    }

    private void OnEnable()
    {
        if (onOnEnable != null)
        {
            onOnEnable.Invoke();
        }
    }

    private void OnDisable()
    {
        if (onOnDisable != null)
        {
            onOnDisable.Invoke();
        }
    }

    private void OnDestroy()
    {
        if (onOnDestroy != null)
        {
            onOnDestroy.Invoke();
        }
    }

    private void Update()
    {
        if (onUpdate != null)
        {
            onUpdate.Invoke();
        }
    }

    private void LateUpdate()
    {
        if (onLateUpdate != null)
        {
            onLateUpdate.Invoke();
        }
    }

    private void FixedUpdate()
    {
        if (onFixedUpdate != null)
        {
            onFixedUpdate.Invoke();
        }
    }
}
