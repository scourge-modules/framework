using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Security;
using System.Linq;
using UnityEngine;

public abstract class ScriptableDataHolder : ScriptableObject
{
	[ContextMenu("Generate Scriptables")]
	protected void GenerateFiles()
	{
		Type myType = this.GetType();
		MemberInfo[] myMemberInfo = myType.GetFields();

		for (int i = 0; i < myMemberInfo.Length; i++)
		{
			if (!(myMemberInfo[i] is PropertyInfo))
			{
				FieldInfo fieldInfo = (FieldInfo)myMemberInfo[i];
				Type mt = fieldInfo.FieldType;

				if (mt.IsSubclassOf(typeof(VariableReference)))
				{
					MemberInfo[] subMemberInfo = mt.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
					object o = fieldInfo.GetValue(this);

					for (int j = 0; j < subMemberInfo.Length; j++)
					{
						if (!(subMemberInfo[j] is PropertyInfo))
						{
							FieldInfo subfieldInfo = (FieldInfo)subMemberInfo[j];
							if (subfieldInfo.Name == "UseScriptable")
							{
								subfieldInfo.SetValue(o, true);
							}
							if (subfieldInfo.Name == "scriptable")
							{
								var asset = ScriptableObject.CreateInstance(subfieldInfo.FieldType);
								asset.name = name + "_" + fieldInfo.Name;
								UnityEditor.AssetDatabase.AddObjectToAsset(asset, this);

								subfieldInfo.SetValue(o, asset);
								UnityEditor.AssetDatabase.SaveAssets();
								UnityEditor.AssetDatabase.Refresh();
							}
							Type smt = subfieldInfo.FieldType;
						}
					}
				}

			}
		}
	}

}
