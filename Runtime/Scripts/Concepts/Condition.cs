﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Condition
{
    public enum LogicalOperator { LessThan, LessThanOrEqual, Equal, GreaterThanOrEqual, GreaterThan };
    public Expression[] Conditions;

    public bool IsMet()
    {
        for (int i = 0; i < Conditions.Length; i++)
            if (!Conditions[i].IsMet())
                return false;

        return true;
    }

    [System.Serializable]
    public class Expression
    {
        public FormattableObject LeftVariable;
        public BoolReference Not;
        public LogicalOperator Operator;
        public StringReference RightVariable;

        public bool IsMet()
        {
            return Not.Value || IsExpressionTrue();
        }

        private bool IsExpressionTrue()
        {
            string leftString = LeftVariable.ToString();
            string rightString = RightVariable.ToString();

            if (Operator == LogicalOperator.Equal)
            {
                return leftString.Trim() == rightString.Trim();
            }

            float leftValue = float.Parse(leftString);
            float rightValue = float.Parse(rightString);

            switch (Operator)
            {
                case LogicalOperator.LessThan:
                    return leftValue < rightValue;
                case LogicalOperator.LessThanOrEqual:
                    return leftValue <= rightValue;
                case LogicalOperator.Equal:
                    return leftString == rightString;
                case LogicalOperator.GreaterThanOrEqual:
                    return leftValue >= rightValue;
                case LogicalOperator.GreaterThan:
                    return leftValue > rightValue;

            }
            return false;
        }
    }
}
