﻿using System.Linq;
using UnityEngine;

public abstract class ScriptableSingleton<T> : ScriptableObject where T : ScriptableObject
{
    protected static T m_instance = null;

    public static T Instance
    {
        get
        {
            if (!m_instance)
                m_instance = Resources.FindObjectsOfTypeAll<T>().FirstOrDefault();
            return m_instance;
        }
    }
}