using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

[System.Serializable]
public class SceneField
{
#if UNITY_EDITOR
    public SceneAsset sceneAsset = null;
#endif
    public string sceneString = "";

    public void Load(LoadSceneMode mode = LoadSceneMode.Additive)
    {
        Debug.Log("loading scene " + sceneString);
        SceneManager.LoadSceneAsync(sceneString, mode);
    }

    public void Unload()
    {
        Debug.Log("unloading scene " + sceneString);
        SceneManager.UnloadSceneAsync(sceneString);
    }
}