﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class CameraRaycast
{
	public static RaycastHit GetPhysicsHit(Vector3 point)
	{
		Ray ray = Camera.main.ScreenPointToRay(point);
		RaycastHit hit;
        Physics.Raycast(ray, out hit);
		return hit;
	}

    public static RaycastHit GetMouseHit()
    {
        return GetPhysicsHit(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
    }
}
