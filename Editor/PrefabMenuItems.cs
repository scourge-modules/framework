﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class PrefabMenuItems : MonoBehaviour
{
    [MenuItem("Core/Prefab Tools/Create Previews")]
    public static void CreateAssetPreviews()
    {
        Object[] selections = Selection.objects;

        for (int i = 0; i < selections.Length; i++)
        {
            Texture2D prev = AssetPreview.GetAssetPreview(selections[i]);

            if (prev != null)
            {
                string prefabPath = AssetDatabase.GetAssetPath(selections[i]);
                FileInfo prefabInfo = new FileInfo(prefabPath);
                string pngName = prefabInfo.FullName.Replace(".prefab", ".png");
                Debug.Log(pngName);

                File.WriteAllBytes(pngName, prev.EncodeToPNG());
                AssetDatabase.Refresh();

            }
        }
    }
}
