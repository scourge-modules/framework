﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class LayoutHelpers
{
    public static List<string> StringListField(List<string> strings)
    {
        int indexToRemove = -1;
        for (int i = 0; i < strings.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            strings[i] = EditorGUILayout.DelayedTextField(strings[i]);
            if (GUILayout.Button("remove"))
            {
                indexToRemove = i;
            }
            EditorGUILayout.EndHorizontal();
        }
        if (indexToRemove != -1)
        {
            strings.RemoveAt(indexToRemove);
        }

        EditorGUI.BeginChangeCheck();
        // GUI.SetNextControlName("prpr");

        string currentMeshKey = EditorGUILayout.DelayedTextField("");
        if (EditorGUI.EndChangeCheck())
        {
            strings.Add(currentMeshKey);
            // EditorGUI.FocusTextInControl("prpr");

        }

        return strings;
    }

    public static List<SerializedProperty> PropertyListField(List<SerializedProperty> props)
    {
        int indexToRemove = -1;
        for (int i = 0; i < props.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(props[i]);
            if (GUILayout.Button("remove"))
            {
                indexToRemove = i;
            }
            EditorGUILayout.EndHorizontal();
        }
        if (indexToRemove != -1)
        {
            props.RemoveAt(indexToRemove);
        }

        EditorGUI.BeginChangeCheck();
        // GUI.SetNextControlName("prpr");

        // SerializedProperty currentProperty = EditorGUILayout.PropertyField();
        // if (EditorGUI.EndChangeCheck())
        // {
        // 	props.Add(currentProperty);
        // 	// EditorGUI.FocusTextInControl("prpr");

        // }

        return props;
    }

    public static List<T> ObjectListField<T>(List<T> elements) where T : UnityEngine.Object
    {
        int indexToRemove = -1;
        for (int i = 0; i < elements.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            elements[i] = EditorGUILayout.ObjectField(elements[i], typeof(T), false) as T;
            if (GUILayout.Button("remove"))
            {
                indexToRemove = i;
            }
            EditorGUILayout.EndHorizontal();
        }
        if (indexToRemove != -1)
        {
            elements.RemoveAt(indexToRemove);
        }

        EditorGUI.BeginChangeCheck();
        T current = null;
        current = EditorGUILayout.ObjectField(current, typeof(T), false) as T;
        if (EditorGUI.EndChangeCheck())
        {
            elements.Add(current);
        }

        return elements;
    }

    public static bool ConditionalButton(bool condition, string nameIfTrue, string nameIfFalse)
    {
        if (condition)
        {
            return GUILayout.Button(nameIfTrue);
        }
        else
        {
            EditorGUILayout.LabelField(nameIfFalse);
            return false;
        }
    }
}