using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(SceneField))]
public class SceneDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);

		EditorGUI.BeginChangeCheck();
		EditorGUI.PropertyField(position, property.FindPropertyRelative("sceneAsset"), label);
		if (EditorGUI.EndChangeCheck())
		{
			property.FindPropertyRelative("sceneString").stringValue = property.FindPropertyRelative("sceneAsset").objectReferenceValue.name;
		}

		EditorGUI.EndProperty();
	}
}