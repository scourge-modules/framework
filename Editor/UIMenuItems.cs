﻿#if UNITY_EDITOR //TODO not needed if in editor foldre where it belongs
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class UIMenuItems : MonoBehaviour
{
    [MenuItem("Core/Sprite Tools/NewCircle/256")]
    private static void MakeCircle256()
    {
        MakeCircle(256, 5);
    }
    private static void MakeCircle(int diameter, int padding)
    {
        Texture2D newTex = new Texture2D(diameter, diameter);
        Color[] pixels = new Color[diameter * diameter];

        int x = 0;
        int y = 0;

        for (int i = 0; i < diameter; i++)
        {
            for (int j = 0; j < diameter; j++)
            {
                x = i - diameter / 2;
                y = j - diameter / 2;
                pixels[i * diameter + j] = Mathf.Sqrt(x * x + y * y) > ((diameter / 2) + padding) ? Color.black : Color.white;
            }
        }

        newTex.SetPixels(pixels);
        newTex.Apply();

        byte[] bytes = newTex.EncodeToPNG();
        System.IO.File.WriteAllBytes(Application.dataPath + "/newTex.png", bytes);
        AssetDatabase.Refresh();

    }

    [MenuItem("Core/Canvas Tools/Remove Raycast %#RIGHT")]
    private static void SetRaycast()
    {
        Undo.RecordObjects(Selection.gameObjects, "Remove Raycast");
        foreach (GameObject GO in Selection.gameObjects)
        {
            Selectable S = GO.GetComponent(typeof(Selectable)) as Selectable;

            SetRaycast(GO, S != null);
        }
    }

    private static void SetRaycast(GameObject GO, bool isSelectable)
    {
        foreach (MaskableGraphic MG in GO.GetComponents(typeof(MaskableGraphic)))
        {
            MG.raycastTarget = isSelectable;
        }
    }

    [MenuItem("Core/Canvas Tools/Remove Raycasts in Children %&#RIGHT")]
    private static void RemoveRaycastChildren()
    {
        if (Selection.gameObjects.Length != 1)
        {
            Debug.LogWarning("Select a single GameObject for this operation.");
            return;
        }

        Undo.RegisterFullObjectHierarchyUndo(Selection.activeGameObject, "Remove UI Raycasts in children");
        RemoveRaycastChildren(Selection.activeGameObject);
    }

    private static void RemoveRaycastChildren(GameObject GO)
    {
        Selectable S = GO.GetComponent(typeof(Selectable)) as Selectable;

        SetRaycast(GO, S != null);

        for (int i = 0; i < GO.transform.childCount; i++)
        {
            RemoveRaycastChildren(GO.transform.GetChild(i).gameObject);
        }
    }

    [MenuItem("Core/Canvas Tools/Set All Text To Align With Geometry")]
    private static void AlignGeometryInChildren()
    {
        if (Selection.gameObjects.Length != 1)
        {
            Debug.LogWarning("Select a single GameObject for this operation.");
            return;
        }

        Undo.RegisterFullObjectHierarchyUndo(Selection.activeGameObject, "Set All Text To Align With Geometry");
        AlignGeometryInChildren(Selection.activeGameObject);
    }

    private static void AlignGeometryInChildren(GameObject GO)
    {
        Text T = GO.GetComponent(typeof(Text)) as Text;
        if (T != null)
        {
            T.alignByGeometry = true;

        }

        for (int i = 0; i < GO.transform.childCount; i++)
        {
            AlignGeometryInChildren(GO.transform.GetChild(i).gameObject);
        }
    }

    [MenuItem("Core/Canvas Tools/Fit Anchors To Self %#DOWN")]
    private static void AnchorsToSelf()
    {
        Undo.RecordObjects(Selection.transforms, "Fit Anchors to Self");
        foreach (RectTransform RT in Selection.transforms)
        {
            AnchorsToSelf(RT);
        }
    }

    private static void AnchorsToSelf(RectTransform RT)
    {
        if (RT.parent != null && (RectTransform)RT.parent != null)
        {
            RectTransform PRT = RT.parent as RectTransform;

            RT.anchorMin = new Vector2(RT.anchorMin.x + RT.offsetMin.x / PRT.rect.width, RT.anchorMin.y + RT.offsetMin.y / PRT.rect.height);
            RT.anchorMax = new Vector2(RT.anchorMax.x + RT.offsetMax.x / PRT.rect.width, RT.anchorMax.y + RT.offsetMax.y / PRT.rect.height);

            RT.offsetMin = RT.offsetMax = new Vector2(0f, 0f);
        }
    }

    [MenuItem("Core/Canvas Tools/Anchors To Self in Children %&#DOWN")]
    private static void AnchorsToSelfChildren()
    {
        if (Selection.gameObjects.Length != 1)
        {
            Debug.LogWarning("Select a single GameObject for this operation.");
            return;
        }

        Undo.RegisterFullObjectHierarchyUndo(Selection.activeGameObject, "Anchors To Self in Children");
        AnchorsToSelfChildren(Selection.activeGameObject.transform as RectTransform);
    }

    private static void AnchorsToSelfChildren(RectTransform RT)
    {
        if (RT != null)
        {
            AnchorsToSelf(RT);
        }
        else
        {
            return;
        }

        Image I = RT.gameObject.GetComponent<Image>();

        if (I != null)
        {
            I.preserveAspect = true;
        }

        for (int i = 0; i < RT.childCount; i++)
        {
            AnchorsToSelfChildren(RT.GetChild(i) as RectTransform);
        }
    }


    [MenuItem("Core/Canvas Tools/Auto Everything %#UP")]
    private static void AutoEverything()
    {
        Undo.RecordObjects(Selection.transforms, "Auto Everything");
        foreach (GameObject GO in Selection.gameObjects)
        {
            AutoEverything(GO);
        }
    }

    private static void AutoEverything(GameObject GO)
    {
        Selectable S = GO.GetComponent(typeof(Selectable)) as Selectable;

        SetRaycast(GO, S != null);

        RectTransform RT = GO.GetComponent<RectTransform>();
        if (RT != null)
        {
            AnchorsToSelf(RT);
        }
    }

    [MenuItem("Core/Canvas Tools/Auto Everything in Children %&#UP")]
    private static void AutoEverythingChildren()
    {
        if (Selection.gameObjects.Length != 1)
        {
            Debug.LogWarning("Select a single GameObject for this operation.");
            return;
        }

        Undo.RegisterFullObjectHierarchyUndo(Selection.activeGameObject, "Auto Everything in Children");
        AutoEverythingChildren(Selection.activeGameObject);
    }

    private static void AutoEverythingChildren(GameObject GO)
    {
        if (GO != null)
        {
            RemoveRaycastChildren(GO);

            RectTransform RT = GO.GetComponent<RectTransform>();
            if (RT != null)
            {
                AnchorsToSelfChildren(RT);
            }
        }
    }

    [MenuItem("Core/Clear PlayerPrefs")]
    private static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
#endif