﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(VariableReference), true)]
[CustomPropertyDrawer(typeof(ListReference), true)]
public class ReferenceDrawer : PropertyDrawer
{
	private readonly string[] popupOptions = { "Use Scriptable", "Use Variable", "Use Component" };
	private GUIStyle popupStyle;
	private SerializedProperty useScriptable;
	private SerializedProperty useComponent;
	private SerializedProperty variable;
	private SerializedProperty scriptable;
	private SerializedProperty component;
	private SerializedProperty propertyToUse;

	private bool isInitialized = false;
	private bool isArrayOrString = false;
	private GUIContent title;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		InitDrawer(property);
		propertyToUse = useComponent.boolValue ? component : useScriptable.boolValue ? scriptable : variable;

		// if (propertyToUse == null)
		// {
		// 	title =
		// }
		// Debug.Log("comp - " + useComponent.boolValue);
		// Debug.Log("scr - " + useScriptable.boolValue);
		isArrayOrString = propertyToUse != null && propertyToUse.isArray && propertyToUse.propertyType != SerializedPropertyType.String;
		title = isArrayOrString ? new GUIContent(new string(' ', property.name.Length)) : label;

		label = EditorGUI.BeginProperty(position, title, property);

		Rect contentPos = EditorGUI.PrefixLabel(position, label); //this makes the label & returns the remaining space

		Rect buttonRect = new Rect(contentPos);
		buttonRect.yMin += popupStyle.margin.top;
		buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
		buttonRect.height = EditorGUIUtility.singleLineHeight;
		contentPos.xMin = buttonRect.xMax;

		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		int scriptableValue = useScriptable.boolValue ? 0 : 1;

		EditorGUI.BeginChangeCheck();
		int result = EditorGUI.Popup(buttonRect, useComponent.boolValue ? 2 : scriptableValue, popupOptions, popupStyle);

		useScriptable.boolValue = result == 0;
		useComponent.boolValue = result == 2;

		if (propertyToUse == null)
		{
			EditorGUI.LabelField(contentPos, "Not serializable");
		}
		else
		{
			if (propertyToUse.hasChildren && propertyToUse.propertyType != SerializedPropertyType.ObjectReference)
			{
				contentPos.x += 10;
				contentPos.width -= 10;
			}
			if (propertyToUse.isArray && propertyToUse.propertyType != SerializedPropertyType.String)
			{
				contentPos.x = position.x;
				contentPos.width = position.width;
				EditorGUI.PropertyField(contentPos, propertyToUse, new GUIContent(property.displayName), propertyToUse.isExpanded);
			}
			else
			{
				if (propertyToUse.propertyType != SerializedPropertyType.Generic)
				{
					EditorGUI.PropertyField(contentPos, propertyToUse, new GUIContent(), true);
				}
				else
				{
					contentPos.y += EditorGUIUtility.singleLineHeight;
					contentPos.height -= EditorGUIUtility.singleLineHeight;
					contentPos.x = position.x;
					contentPos.width = position.width;
					EditorGUI.PropertyField(contentPos, propertyToUse, new GUIContent(), true);
				}
			}
		}

		if (EditorGUI.EndChangeCheck())
			property.serializedObject.ApplyModifiedProperties();

		EditorGUI.indentLevel = indent;


		EditorGUI.EndProperty();
	}

	private void InitDrawer(SerializedProperty property)
	{
		if (!isInitialized)
		{
			popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
			popupStyle.imagePosition = ImagePosition.ImageOnly;

			useScriptable = property.FindPropertyRelative("UseScriptable");
			useComponent = property.FindPropertyRelative("UseComponent");
			variable = property.FindPropertyRelative("variable");
			scriptable = property.FindPropertyRelative("scriptable");
			component = property.FindPropertyRelative("component");
		}
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		SerializedProperty useScriptable = property.FindPropertyRelative("UseScriptable");
		SerializedProperty useComponent = property.FindPropertyRelative("UseComponent");
		SerializedProperty variable = property.FindPropertyRelative("variable");
		SerializedProperty scriptable = property.FindPropertyRelative("scriptable");
		SerializedProperty component = property.FindPropertyRelative("component");
		SerializedProperty toUse = useScriptable.boolValue ? scriptable : variable;
		if (useComponent != null && useComponent.boolValue)
		{
			toUse = component;
		}
		// Debug.LogError(variable);
		// Debug.LogError(scriptable);
		// Debug.LogError(component);
		// Debug.LogError(toUse);

		float genericOffset = 0f;

		if (toUse == null)
		{
			return EditorGUIUtility.singleLineHeight;
		}

		genericOffset = toUse.propertyType == SerializedPropertyType.Generic ? EditorGUIUtility.singleLineHeight : 0f;

		if (toUse.isArray)
		{
			genericOffset = 0f;
		}
		return EditorGUI.GetPropertyHeight(toUse, GUIContent.none, true) + genericOffset;
	}


}