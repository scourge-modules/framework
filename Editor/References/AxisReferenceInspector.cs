﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using Scourge;

[CustomPropertyDrawer(typeof(AxisReference))]
public class AxisReferenceInspector : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty indexProp = property.FindPropertyRelative("index");
        SerializedProperty valueProp = property.FindPropertyRelative("Value");
        if (indexProp != null && valueProp != null)
        {
            if (Axis.Instance != null)
            {
                string[] names = Axis.Instance.all.Select(x => x.Value).ToArray();
                indexProp.intValue = EditorGUI.Popup(position, indexProp.intValue, names);
                valueProp.objectReferenceValue = Axis.Instance.all[indexProp.intValue];
            }
            else
            {
                Debug.LogError("No Axis Instance");
            }
        }
    }
}