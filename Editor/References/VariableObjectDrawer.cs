﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// TODO please explain this class - reference drawer seems to do everything, and it only seems to break things occasionally

// [CustomPropertyDrawer(typeof(ScriptableBool)), CustomPropertyDrawer(typeof(ScriptableFloat))]
// [CustomPropertyDrawer(typeof(ScriptableInt)), CustomPropertyDrawer(typeof(ScriptableString))]
// [CustomPropertyDrawer(typeof(FormattableObject))]
public class VariableObjectDrawer : PropertyDrawer
{
    private Object scriptable;
    private SerializedObject serializedObject;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (scriptable == null) scriptable = property.objectReferenceValue as ScriptableBool;
        if (scriptable == null) scriptable = property.objectReferenceValue as ScriptableInt;
        if (scriptable == null) scriptable = property.objectReferenceValue as ScriptableFloat;
        if (scriptable == null) scriptable = property.objectReferenceValue as ScriptableString;

        if (serializedObject == null && scriptable != null) serializedObject = new SerializedObject(scriptable);

        label = EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, label);



        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect scriptableContainer = new Rect(position);
        if (scriptable != null)
        {
            SerializedProperty valueProp = serializedObject.FindProperty("value");
            Rect valueContainer = new Rect(position);

            int fifth = (int)(position.width * 0.2f);

            scriptableContainer.width -= fifth;
            scriptableContainer.x += fifth;
            valueContainer.width = fifth;

            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(valueContainer, valueProp, GUIContent.none);
            if (EditorGUI.EndChangeCheck())
                serializedObject.ApplyModifiedProperties();
        }

        EditorGUI.BeginChangeCheck();
        EditorGUI.PropertyField(scriptableContainer, property, GUIContent.none);
        if (EditorGUI.EndChangeCheck())
            property.serializedObject.ApplyModifiedProperties();

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }
}