using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CodeGeneration : EditorWindow
{
	private string className;
	private bool generateUnityEvent = true;
	private bool generateReference = true;
	private bool generateEventReference = true;
	private bool generateEvent = true;
	private bool generateListComponent = true;
	private bool generateListReference = true;
	private bool generateScriptable = true;
	private bool generateComponent = true;
	private bool generateScriptableList = true;
	private bool generateInitializer = false;
	private bool isUpperCase = true;

	[MenuItem("Core/New Reference")]
	private static void Init()
	{
		// Get existing open window or if none, make a new one:
		CodeGeneration window = (CodeGeneration)EditorWindow.GetWindow(typeof(CodeGeneration));
		window.Show();
	}

	private void OnGUI()
	{
		className = EditorGUILayout.TextField("ClassName", className);
		isUpperCase = EditorGUILayout.Toggle("Upper case first character of class", isUpperCase);
		generateUnityEvent = EditorGUILayout.Toggle("Unity Event", generateUnityEvent);
		generateReference = EditorGUILayout.Toggle("Reference", generateReference);
		generateEventReference = EditorGUILayout.Toggle("Event Reference", generateEventReference);
		generateListReference = EditorGUILayout.Toggle("List Reference", generateListReference);
		generateScriptable = EditorGUILayout.Toggle("Scriptable", generateScriptable);
		generateComponent = EditorGUILayout.Toggle("Component", generateComponent);
		generateScriptableList = EditorGUILayout.Toggle("Scriptable List", generateScriptableList);
		generateInitializer = EditorGUILayout.Toggle("Initializer", generateInitializer);
		generateEvent = EditorGUILayout.Toggle("Event", generateEvent);
		generateListComponent = EditorGUILayout.Toggle("List Component", generateListComponent);

		if (GUILayout.Button("Generate Files"))
		{
			string parentFolder = AssetDatabase.CreateFolder("Assets", className);
			Debug.LogError(parentFolder);

			string classNameToLower = isUpperCase ? className : className.ToLower();
			if (generateUnityEvent)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, className + "UnityEvent.cs");
				string cnt = UNITY_EVENT_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateComponent)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, className + "Component.cs");
				string cnt = COMPONENT_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateEventReference)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, className + "EventReference.cs");
				string cnt = EVENT_REFERENCE_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateReference)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, className + "Reference.cs");
				string cnt = REFERENCE_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateScriptable)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, "Scriptable" + className + ".cs");
				string cnt = SCRIPTABLE_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateScriptableList)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, "Scriptable" + className + "List.cs");
				string cnt = SCRIPTABLE_LIST_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateListReference)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, className + "ListReference.cs");
				string cnt = LIST_REFERENCE_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateListReference)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, className + "Event.cs");
				string cnt = EVENT_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateListComponent)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, className + "ListComponent.cs");
				string cnt = LIST_COMPONENT.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			if (generateInitializer)
			{
				string pth = Path.Combine(Path.GetFullPath(Application.dataPath), className, className + "ReferenceInitializer.cs");
				string cnt = INITIALIZER_CODE.Replace("{0}", className).Replace("{1}", classNameToLower);
				File.WriteAllText(pth, cnt);
			}
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
	}

	private const string LIST_COMPONENT = @"using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class {0}ListComponent : ComponentListVariable<{1}>
{
}
";

	private const string EVENT_CODE = @"using System;
using UnityEngine;

public class {0}Event : GameEvent {
    public new event Action<{1}> Event;

#if UNITY_EDITOR
    public {1} TestValue;

    [ContextMenu(""Raise as {0}"")]
    public override void EditorRaise()
	{
		Raise(TestValue);
	}
#endif

	public void Raise({1} value)
	{
		base.Raise();
		Event(value);
	}
}
";

	private const string INITIALIZER_CODE = @"using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class {0}ReferenceInitializer : MonoBehaviour
{
    [SerializeField] private {0}Reference reference;
    [SerializeField] private {1} target;

    private void Awake()
    {
        reference.Value = target;
    }
}
";

	private const string LIST_REFERENCE_CODE = @"using System;
using UnityEngine;

[Serializable]

public class {0}ListReference : ListReference<{1}>
{
        
}";

	private const string SCRIPTABLE_LIST_CODE = @"using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ""New Static/List/String"")]
public class Scriptable{0}List : ScriptableList<{1}>
	{
	}
";

	private const string UNITY_EVENT_CODE = @"using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class {0}UnityEvent : UnityEvent<{1}>
{
}
";

	private const string COMPONENT_CODE = @"using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class {0}Component : ComponentVariable<{1}>
{
    
}
";
	private const string REFERENCE_CODE = @"using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class {0}Reference : VariableReference<{1}, Scriptable{0}>
{
}";
	private const string EVENT_REFERENCE_CODE = @"using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class {0}EventReference : VariableReference
{
    public bool UseScriptable;
    public {0}Event scriptable;
    public {0}UnityEvent variable;

    public void Raise({1} f)
    {
        if (UseScriptable)
        {
            if (scriptable != null)
            {
                scriptable.Raise(f);
            }
            else
            {
                Debug.LogError(""Event was null, skipping for now"");
            }
        }

		else
{
	variable.Invoke(f);
}
    }
}
";

	private const string SCRIPTABLE_CODE = @"using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = ""New Static/Variable/{0}"")]
public class Scriptable{0} : VariableObject<{1}>
	{
		public override string ToString(string format, IFormatProvider formatProvider)
		{
			return Value.ToString(format, formatProvider);
		}

		public void SetValue({1} f)
		{
			Value = f;
		}

	}";
}
