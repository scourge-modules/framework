﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEditor;
using UnityEngine;

// a bit hacky and with some errors Im not supressing 
// need chained dependencies
// should use a tree and leaf approach
// better moving hierarcy of branches

public class LegacyModuleEditorWindow : EditorWindow
{
    private class Branch
    {
        public string FullName; // module/category1/category2/moduleName
        public string ShortName; // moduleName
        public string PascalName;
        public string Category = "General"; //should be a string
        public bool IsActive;
        public bool MarkedForActive;

        public Branch(string fullName, bool isActive)
        {
            FullName = fullName.Trim();
            IsActive = isActive;
            MarkedForActive = IsActive;

            int index = FullName.LastIndexOf('/');

            ShortName = FullName;

            if (index > 0)
            {
                Category = FullName.Substring(0, index).Trim();
                ShortName = FullName.Substring(index + 1, FullName.Length - index - 1).Trim();
            }

            if (!string.IsNullOrEmpty(ShortName))
            {
                char[] letters = ShortName.ToCharArray();
                letters[0] = char.ToUpper(letters[0]);
                PascalName = new string(letters);
            }

            Category = Category.Replace("module/", "");
            Category = Category.Replace("module", "");
            if (string.IsNullOrWhiteSpace(Category))
            {
                Category = "General";
            }

            if (!string.IsNullOrEmpty(Category))
            {
                char[] letters = Category.ToCharArray();
                letters[0] = char.ToUpper(letters[0]);
                Category = new string(letters);
            }
        }

        public void UpdateState()
        {
            if (IsActive != MarkedForActive)
            {
                if (MarkedForActive)
                {
                    (FullName + " will now get checked out").LR();
                    string args = Application.dataPath + "/Core/" + PascalName + " " + FullName;
                    ExternalScripts.Run("git_checkout_worktree.sh", args, callback: FinishCheckout);
                }
                else
                {
                    (FullName + " will now get removed").LR();
                    ExternalScripts.Run("git_remove_worktree.sh", Application.dataPath + "/Core/" + PascalName + " " + FullName, callback: FinishRemove);

                }
                //should only set this on the callback
                IsActive = MarkedForActive;
            }
        }

        private void FinishCheckout()
        {
            AssetDatabase.Refresh();
        }

        private void FinishRemove()
        {
            AssetDatabase.DeleteAsset("Assets/Core/" + PascalName);
            AssetDatabase.Refresh();
        }

        public override bool Equals(object obj)
        {
            return obj is Branch && this == (Branch)obj;
        }
        public override int GetHashCode()
        {
            return FullName.GetHashCode();
        }
        public static bool operator ==(Branch x, Branch y)
        {
            return x.FullName == y.FullName;
        }
        public static bool operator !=(Branch x, Branch y)
        {
            return !(x == y);
        }
    }

    [MenuItem("Core/Modules")]
    public static void ShowWindow()
    {
        LegacyModuleEditorWindow window = (LegacyModuleEditorWindow)EditorWindow.GetWindow(typeof(LegacyModuleEditorWindow));
        window.Refresh();
    }

    // key = origin / bank 
    // value = special_bank, normal_bank, experimental_bank
    private Dictionary<string, Dictionary<string, Branch>> branchInfo = new Dictionary<string, Dictionary<string, Branch>>();
    private bool finishedRefreshing = false;
    private bool finishedGettingBranches = false;
    private bool finishedGettingWorktrees = false;
    private bool openForNewBranch = false;
    private string newModuleName = string.Empty;
    private Vector2 scrollPos;

    private void OnFocus()
    {
        Refresh();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Refresh"))
        {
            Refresh();
        }

        if (finishedRefreshing) // response from all c# processes after refresh
        {
            if (branchInfo.Count > 0)
            {
                scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
                foreach (string category in branchInfo.Keys)
                {
                    EditorGUILayout.LabelField(category, EditorStyles.boldLabel);
                    Dictionary<string, Branch> categoryValues = branchInfo[category];
                    foreach (string branchName in categoryValues.Keys)
                    {
                        Branch branch = categoryValues[branchName];
                        string title = (branch.IsActive) ? "* " + branch.PascalName : branch.PascalName;
                        branch.MarkedForActive = EditorGUILayout.Toggle(title, branch.MarkedForActive);
                    }
                    EditorGUILayout.Space();
                }

                EditorGUILayout.EndScrollView();


                if (GUILayout.Button("Confirm Selection"))
                {
                    List<Branch> branches = branchInfo.Values.SelectMany(x => x.Values).ToList();

                    foreach (Branch branch in branches)
                    {
                        branch.UpdateState();
                    }
                }

            }
            if (!openForNewBranch && GUILayout.Button("New module"))
            {
                openForNewBranch = true;

            }
            if (openForNewBranch)
            {
                newModuleName = EditorGUILayout.TextField("New Module Name: ", newModuleName);
                if (GUILayout.Button("Create"))
                {
                    openForNewBranch = false;
                    ExternalScripts.Run("git_create_blank_branch.sh", "module/" + newModuleName, callback: Refresh);
                    newModuleName = string.Empty;
                }

            }

        }

    }

    private void Refresh()
    {
        finishedRefreshing = false;
        finishedGettingBranches = false;
        finishedGettingWorktrees = false;

        branchInfo.Clear();

        ExternalScripts.Run("git_branch_list.sh", outputHandler: BranchHandler, callback: OnBranchesListed);
        ExternalScripts.Run("git_worktree_branch.sh", outputHandler: WorktreeHandler, callback: OnWorktreesListed);
    }

    private void WorktreeHandler(object sendingProcess, DataReceivedEventArgs outLine)
    {
        try
        {
            if (outLine.Data != null && !outLine.Data.Contains("->"))
            {
                string left = outLine.Data.Split('[').Last();
                string worktree = left.Split(']').First().Trim();
                worktree = worktree.Replace("* ", "").Trim();

                if (worktree.StartsWith("module/"))
                {
                    Branch val = new Branch(worktree, true);

                    if (!branchInfo.ContainsKey(val.Category))
                    {
                        branchInfo.Add(val.Category, new Dictionary<string, Branch>());
                    }

                    if (!branchInfo[val.Category].ContainsKey(val.ShortName))
                    {
                        branchInfo[val.Category].Add(val.ShortName, val);
                    }
                    else
                    {
                        branchInfo[val.Category][val.ShortName].IsActive = true;
                        branchInfo[val.Category][val.ShortName].MarkedForActive = true;
                    }

                }


            }

        }
        catch (System.Exception e)
        {
            UnityEngine.Debug.LogError(e);
        }
    }

    private void OnBranchesListed()
    {
        // UnityEngine.Debug.Log("All branches listed");
        finishedGettingBranches = true;
        if (finishedGettingWorktrees)
        {
            OnFinishRefresh();
        }
    }

    private void OnWorktreesListed()
    {
        // UnityEngine.Debug.Log("All worktrees listed");
        finishedGettingWorktrees = true;
        if (finishedGettingBranches)
        {
            OnFinishRefresh();
        }
    }

    private void OnFinishRefresh()
    {
        finishedRefreshing = true;
        // UnityEngine.Debug.Log("finished refreshing");
    }

    private void BranchHandler(object sendingProcess, DataReceivedEventArgs outLine)
    {
        try
        {

            if (outLine.Data != null && !outLine.Data.Contains("->"))
            {
                //if theres only 1 category, put it in a "general" category else categorize based on /
                // key = origin / bank 
                // value = special_bank, normal_bank, experimental_bank
                // 
                // if the category doesnt exist create it, else add the branch to the existing category

                string branch = outLine.Data.Replace("remotes/origin/", "").Trim();
                branch = branch.Replace("* ", "");

                //TODO shouldnt be contains
                if (branch.StartsWith("module/"))
                {
                    Branch val = new Branch(branch, false);

                    if (!branchInfo.ContainsKey(val.Category))
                    {
                        branchInfo.Add(val.Category, new Dictionary<string, Branch>());
                    }

                    if (!branchInfo[val.Category].ContainsKey(val.ShortName))
                    {
                        branchInfo[val.Category].Add(val.ShortName, val);
                    }

                }

            }

        }
        catch (System.Exception e)
        {
            UnityEngine.Debug.LogError(e);
        }
    }

}
