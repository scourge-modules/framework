﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Diagnostics;
using System.Linq;

public class ModuleMenuItems : MonoBehaviour
{
    private static bool moduleValid = true;
    private static string moduleName = string.Empty;

    [MenuItem("Assets/Modules/Convert To Package")]
    private static void ConvertToPackage()
    {
        moduleValid = true;
        string filePath = AssetDatabase.GetAssetPath(Selection.activeObject);
        moduleName = filePath.Substring(7).ToLower().Trim();
        ExternalScripts.Run("checkPackageValid.ps1", "\'" + moduleName + "\'", PackageCheckHandler, MakeNewRepo);
    }

    private static void PackageCheckHandler(object sendingProcess, DataReceivedEventArgs outLine)
    {
        if (outLine.Data.StartsWith("module/" + moduleName) || outLine.Data.Replace("/", "_").StartsWith("module_" + moduleName))
        {
            moduleValid = false;
            UnityEngine.Debug.LogError(moduleName + "already exists! exiting...");
        }

    }

    private static void NewRepoHandler(object sendingProcess, DataReceivedEventArgs outLine)
    {
        UnityEngine.Debug.LogError(outLine.Data);
        // try
        // {
        // }
    }

    private static void MakeNewRepo()
    {
        if (moduleValid)
        {
            ExternalScripts.Run("makeNewRepo.ps1", "\'" + moduleName + "\'", NewRepoHandler, PullRepo);
        }
        else
        {
            UnityEngine.Debug.LogError("module check failed");
        }

    }

    private static void PullRepo()
    {
        if (moduleValid)
        {
            ExternalScripts.Run("pullRepo.ps1", "\'" + moduleName + "\'", null, MoveFilesToRepoDir);
        }
        else
        {
            UnityEngine.Debug.LogError("module check failed");
        }

    }

    private static void MoveFilesToRepoDir()
    {
        UnityEngine.Debug.Log("Moving " + moduleName);
        string packageName = Path.Combine(Application.dataPath.Replace("Assets", "Packages"), moduleName.Replace("/", "_"), "Runtime");
        string assetsName = Path.Combine(Application.dataPath, moduleName);

        Directory.Move(assetsName, packageName);

        string packagePath = Path.Combine(new DirectoryInfo(packageName).Parent.FullName, "package.json");
        string packageJson = "{\r\n  \"name\": \"com.scourge." + moduleName.Replace("/", "_") + "\",\r\n  \"version\": \"1.0.0\",\r\n  \"displayName\": \"" + moduleName + "\",\r\n  \"description\": \"new module\",\r\n  \"author\": {\r\n    \"name\": \"Scourge\",\r\n    \"email\": \"scourgeoa@gmail.com\",\r\n    \"url\": \"https://bitbucket.org/scourge-modules" + moduleName + "\"\r\n  },\r\n  \"hideInEditor\": false,\r\n  \"unity\": \"2019.4\",\r\n  \"dependencies\": {\r\n    \"com.scourge.module_framework\": \"1.0.0\"\r\n  }\r\n}";

        File.WriteAllText(packagePath, packageJson);

        AssetDatabase.Refresh();
        // ExternalScripts.Run("makeNewRepo.ps1", "\'" + moduleName + "\'", NewRepoHandler, MoveFilesToRepoDir);

    }

    [MenuItem("Assets/Modules/Convert To Package", true)]
    private static bool ValidateAssetFolder()
    {
        string filePath = AssetDatabase.GetAssetPath(Selection.activeObject);

        bool isAsset = filePath != null && filePath.StartsWith("Assets/");
        if (isAsset)
        {
            FileAttributes attr = File.GetAttributes(filePath);
            bool isFolder = ((attr & FileAttributes.Directory) == FileAttributes.Directory);
            return (isFolder || Application.platform == RuntimePlatform.LinuxEditor);
        }

        return false;
    }

    [MenuItem("Assets/Modules/Update Package")]
    private static void UpdateCustomPackage()
    {
        AssetDatabase.Refresh();
        AssetDatabase.SaveAssets();
        string dirPath = AssetDatabase.GetAssetPath(Selection.activeObject);

        ExternalScripts.Run("updatePackage.ps1", Path.GetFullPath(dirPath));
    }
    [MenuItem("Assets/Modules/Create Assembly Definitions")]
    private static void CreateASM()
    {
        string fullRuntimePath = Path.GetFullPath(AssetDatabase.GetAssetPath(Selection.activeObject) + "/Runtime");
        string fullEditorPath = Path.GetFullPath(AssetDatabase.GetAssetPath(Selection.activeObject) + "/Editor");
        string packageName = Selection.activeObject.name.Split('.').Last();

        if (Directory.Exists(fullRuntimePath))
        {
            string defName = "Scourge." + packageName[0].ToString().ToUpper() + packageName.Substring(1) + ".Runtime";
            string defPath = fullRuntimePath + "/" + defName + ".asmdef";
            string content = "{\r\n    \"name\": \"" + defName + "\",\r\n    \"references\": [\r\n        \"GUID:c0a17d3351972a847a3ec76601120917\"\r\n    ],\r\n    \"includePlatforms\": [],\r\n    \"excludePlatforms\": [],\r\n    \"allowUnsafeCode\": false,\r\n    \"overrideReferences\": false,\r\n    \"precompiledReferences\": [],\r\n    \"autoReferenced\": true,\r\n    \"defineConstraints\": [],\r\n    \"versionDefines\": [],\r\n    \"noEngineReferences\": false\r\n}";
            if (!File.Exists(defPath))
            {
                File.WriteAllText(defPath, content);
            }

        }
        if (Directory.Exists(fullEditorPath))
        {
            string defName = "Scourge." + packageName[0].ToString().ToUpper() + packageName.Substring(1) + ".Editor";
            string defPath = fullEditorPath + "/" + defName + ".asmdef";
            string content = "{\n    \"name\": \"" + defName + "\",\n    \"references\": [\n        \"GUID:44c054cb87256164bbe4b6a176b7ca28\"\n    ],\n    \"includePlatforms\": [\n        \"Editor\"\n    ],\n    \"excludePlatforms\": [],\n    \"allowUnsafeCode\": false,\n    \"overrideReferences\": false,\n    \"precompiledReferences\": [],\n    \"autoReferenced\": true,\n    \"defineConstraints\": [],\n    \"versionDefines\": [],\n    \"noEngineReferences\": false\n}";
            if (!File.Exists(defPath))
            {
                File.WriteAllText(defPath, content);
            }

        }

        AssetDatabase.Refresh();
        AssetDatabase.SaveAssets();
    }

    [MenuItem("Assets/Modules/Create Assembly Definitions", true)]
    [MenuItem("Assets/Modules/Update Package", true)]
    private static bool UpdateCustomPackageCheck()
    {
        string dirPath = AssetDatabase.GetAssetPath(Selection.activeObject);

        bool isPackage = dirPath != null && dirPath.StartsWith("Packages/com.scourge");

        if (isPackage)
        {
            FileAttributes attr = File.GetAttributes(dirPath);
            bool isFolder = ((attr & FileAttributes.Directory) == FileAttributes.Directory);
            if (isFolder)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Path.GetFullPath(dirPath));
                return dirInfo.Parent.Name.ToLower() == "packages";
            }
        }

        return false;
    }
}
