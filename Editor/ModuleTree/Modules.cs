﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

//--------------IMPORTANT
// if parent is not groupenabled and all children are groupenabled set parent groupenabled
// push all changes on all branches
// git status on files and display information at the top
// refresh shouldnt collapse everything
// refresh shouldnt make new moduletree
// key not found error
//--------------DREAM
// if branch is called main and its the only branch no need to display differently
// visual updates on changes to a package
// actually rename the bitbucket repo
// tidy the crap

public class Modules : EditorWindow
{
    public ModuleTree moduleTree;

    private List<string> moduleTags = new List<string>();

    [MenuItem("Core/ModuleTree")]
    public static void ShowWindow()
    {
        Modules window = (Modules)EditorWindow.GetWindow(typeof(Modules));

        window.Refresh();
    }

    private void Refresh()
    {
        moduleTags.Clear();
        ExternalScripts.Run("checkPackageValid.ps1", "", PackageCheckHandler, BuildTree);
    }

    private void PackageCheckHandler(object sendingProcess, System.Diagnostics.DataReceivedEventArgs outLine)
    {
        if (outLine.Data.StartsWith("module"))
        {
            moduleTags.Add(outLine.Data.Substring(7));
        }
    }

    public void BuildTree()
    {
        moduleTree = new ModuleTree(Refresh, moduleTags.Where(x => !x.StartsWith("#")).ToArray());
    }

    private void OnGUI()
    {
        if (moduleTree != null)
        {
            moduleTree.OnGUI();
        }

        if (GUILayout.Button("Refresh") || moduleTree == null)
        {
            Refresh();
        }

        if (GUILayout.Button("Apply Changes"))
        {
            moduleTree.ApplyModuleChanges();
        }
    }
}
