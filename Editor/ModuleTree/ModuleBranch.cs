﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class ModuleBranch
{
    private bool groupEnabled = false;
    private bool isSettingsFolded = true;
    private bool isFolded = false;
    private Branch branch;
    private bool addedMapping = false;
    private string renameTarget;
    private bool isInProject = false;
    private System.Action refresh;

    public ModuleBranch(Branch branch, System.Action refresh)
    {
        this.refresh = refresh;
        addedMapping = false;
        isFolded = true;
        this.branch = branch;
    }

    public void SetGroupEnabledIfChildrenEnabled(Dictionary<Branch, ModuleBranch> mapping)
    {
        if (branch.Branches.Count > 0)
        {
            if (branch.Branches.Where(x => mapping[x.Value].groupEnabled).Count() == branch.Branches.Count)
            {
                SetGroupEnabled(mapping[branch.Branches.First().Value].groupEnabled, mapping);
            }
            else
            {
                groupEnabled = false;
            }
        }
    }

    public void SetGroupEnabled(bool enabled, Dictionary<Branch, ModuleBranch> mapping)
    {
        groupEnabled = enabled;

        if (branch.Branches.Count > 0)
        {
            foreach (KeyValuePair<string, Branch> b in branch.Branches)
            {
                mapping[branch.Branches[b.Key]].SetGroupEnabled(groupEnabled, mapping);
            }
        }
    }

    private void GuiLine(int i_height = 1)
    {
        Rect rect = EditorGUILayout.GetControlRect(false, i_height);
        rect.height = i_height;
        rect.y += 1;
        EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 0.25f));
    }


    public void OnGUI(Dictionary<Branch, ModuleBranch> mapping, List<string> branchesInProject)
    {
        if (!addedMapping)
        {
            foreach (KeyValuePair<string, Branch> kvp in branch.Branches)
            {
                if (!mapping.ContainsKey(kvp.Value))
                {
                    mapping.Add(kvp.Value, new ModuleBranch(kvp.Value, refresh));
                }
            }

            if (branch.Branches.Keys.Count == 0)
            {
                if (branchesInProject.Contains(branch.FullName.Substring(5).Replace("/", "_")))
                {
                    isInProject = groupEnabled = true;
                }
                else
                {
                    isInProject = groupEnabled = false;
                }

                // if (branch.Parent != null)
                // {

                //     if (groupEnabled != mapping[branch.Parent].groupEnabled)
                //     {
                //         mapping[branch.Parent].SetGroupEnabledIfChildrenEnabled(mapping);
                //     }
                // }
            }
            addedMapping = true;
        }
        if (branch.Name != "Root")
        {
            EditorGUI.indentLevel = branch.IndentLevel;
            if (branch.Branches.Keys.Count > 0)
            {
                GuiLine();
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space(15, false);
            Rect current = GUILayoutUtility.GetLastRect();
            current.width = 15 + branch.IndentLevel * 15;
            current.height = 15;
            current.y += 2;
            // current.x += branch.IndentLevel * 10;
            if (branch.Branches.Keys.Count > 0)
            {
                isFolded = EditorGUI.Foldout(current, isFolded, "");
            }

            EditorGUI.BeginChangeCheck();
            groupEnabled = EditorGUILayout.BeginToggleGroup(branch.Name, groupEnabled);
            bool enabledChangeCheck = EditorGUI.EndChangeCheck();
            EditorGUILayout.EndToggleGroup();
            if (branch.Branches.Keys.Count == 0)
            {
                EditorGUI.BeginChangeCheck();
                isSettingsFolded = EditorGUILayout.Foldout(isSettingsFolded, "Settings");
                if (EditorGUI.EndChangeCheck())
                {
                    renameTarget = branch.FullName.Substring(5);
                }


            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical();

            if (!isSettingsFolded)
            {
                renameTarget = EditorGUILayout.TextField(renameTarget);
                if (GUILayout.Button("Rename"))
                {
                    if (EditorUtility.DisplayDialog("Rename module?", "Attempting to rename " + branch.FullName.Substring(5) + " to " + renameTarget + ". Proceed?\n You will still have to rename the module to match the new name on BitBucket.", "Continue", "Cancel"))
                    {
                        ExternalScripts.Run("renamePackage.ps1", "\"" + branch.FullName.Substring(5) + "\" \"" + renameTarget + "\"", null, () => { AssetDatabase.Refresh(); refresh(); });
                    }
                }
            }

            if (enabledChangeCheck)
            {
                if (branch.Branches.Count > 0)
                {
                    foreach (KeyValuePair<string, Branch> b in branch.Branches)
                    {
                        mapping[branch.Branches[b.Key]].SetGroupEnabled(groupEnabled, mapping);
                    }
                }

                if (branch.Parent != null)
                {
                    if (groupEnabled != mapping[branch.Parent].groupEnabled)
                    {
                        mapping[branch.Parent].SetGroupEnabledIfChildrenEnabled(mapping);
                    }
                }
            }

            if (isFolded)
            {
                if (branch.Branches.Count > 0)
                {
                    foreach (KeyValuePair<string, Branch> b in branch.Branches)
                    {
                        mapping[branch.Branches[b.Key]].OnGUI(mapping, branchesInProject);
                    }
                }

            }

            EditorGUILayout.EndVertical();
        }
        else
        {
            if (branch.Branches.Count > 0)
            {
                foreach (KeyValuePair<string, Branch> b in branch.Branches)
                {
                    mapping[branch.Branches[b.Key]].OnGUI(mapping, branchesInProject);
                }
            }
        }
    }

    public void ApplyModuleChanges(Dictionary<Branch, ModuleBranch> mapping)
    {
        if (branch.Branches.Count > 0)
        {
            foreach (KeyValuePair<string, Branch> b in branch.Branches)
            {
                try
                {
                    mapping[branch.Branches[b.Key]].ApplyModuleChanges(mapping);
                }
                catch
                {
                    Debug.Log("wtf");
                }
            }
        }
        else
        {
            if (groupEnabled && !isInProject)
            {
                ExternalScripts.Run("pullRepo.ps1", branch.FullName.Substring(5), null, AssetDatabase.Refresh);
            }
            else if (!groupEnabled && isInProject)
            {
                gitSafe = false;
                ExternalScripts.Run("checkDeleteRepo.ps1", branch.FullName.Substring(5).Replace("/", "_"), DeleteRepoCheck, HandleDeleteCheck);
            }
        }
    }

    private void HandleDeleteCheck()
    {
        if (gitSafe)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Path.GetFullPath(Application.dataPath)).Parent;
            FileUtil.DeleteFileOrDirectory(Path.Combine(directoryInfo.FullName, "Packages", branch.FullName.Substring(5).Replace("/", "_")));
            AssetDatabase.Refresh();
        }
        else
        {
            if (EditorUtility.DisplayDialog("Cant remove module", "Module has unstaged changes - please fix this", "Automatic Update", "Cancel"))
            {
                DirectoryInfo projDir = new DirectoryInfo(Application.dataPath).Parent;
                ExternalScripts.Run("updatePackage.ps1", Path.Combine(projDir.FullName, "Packages", branch.FullName.Substring(5).Replace("/", "_")));
            }
        }

    }

    private bool gitSafe = false;

    private void DeleteRepoCheck(object sendingProcess, System.Diagnostics.DataReceivedEventArgs outLine)
    {
        if (outLine.Data.StartsWith("nothing to commit, working tree clean"))
        {
            gitSafe = true;
        }
    }
}
