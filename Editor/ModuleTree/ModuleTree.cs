﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class ModuleTree
{
    private Vector2 scrollviewPos = new Vector2();
    private Tree tree;
    private ModuleBranch root;

    public Tree Tree => tree;

    public Dictionary<Branch, ModuleBranch> mapping = new Dictionary<Branch, ModuleBranch>();

    public List<string> branchesInProject = new List<string>();

    private System.Action refresh;


    public ModuleTree(System.Action refresh, params string[] branchNames)
    {
        this.refresh = refresh;
        CalculateBranchesInProject();
        tree = new Tree(branchNames);
        root = new ModuleBranch(tree.root, refresh);
        mapping.Clear();
    }

    public void OnGUI()
    {
        GUILayout.Label("Module Info", EditorStyles.boldLabel);
        scrollviewPos = GUILayout.BeginScrollView(scrollviewPos);
        if (root != null && mapping != null)
        {
            root.OnGUI(mapping, branchesInProject);
        }
        GUILayout.EndScrollView();
    }

    private void CalculateBranchesInProject()
    {
        branchesInProject.Clear();
        DirectoryInfo dirInfo = new DirectoryInfo(Path.GetFullPath(Application.dataPath));
        dirInfo = dirInfo.Parent.GetDirectories().Where(x => x.Name.ToLower() == "packages").First();

        DirectoryInfo[] packages = dirInfo.GetDirectories();
        for (int i = 0; i < packages.Length; i++)
        {
            branchesInProject.Add(packages[i].Name);
        }
    }

    public void ApplyModuleChanges()
    {
        root.ApplyModuleChanges(mapping);
    }
}
