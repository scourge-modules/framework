﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;

//TODO create enum type instead of checking endswith all the time to determine process directory
//TODO allow that eunm to be inserted into the params of run script to override.
//TODO hook onto errordata
//TODO create subclass instead of passing in everything as params
//TODO handle supressed errors better, or figure out correct fix


public class ExternalScripts
{
    private const string JOB_CONTROL_ERROR = "bash: no job control in this shell";
    private const string IOCTL_ERROR = "bash: cannot set terminal process group (-1): Inappropriate ioctl for device";

    private static string GetCurrentFileName([System.Runtime.CompilerServices.CallerFilePath] string fileName = null)
    {
        return fileName;
    }
    public static void Run(string scriptName, string arguments = null, DataReceivedEventHandler outputHandler = null, System.Action callback = null)
    {
        UnityEngine.Debug.LogFormat("running {0} with arguments {1}", scriptName, arguments);
        UnityEngine.Debug.Log(GetCurrentFileName());
        FileInfo fileInfo = new FileInfo(GetCurrentFileName());
        // DirectoryInfo parentDir = new DirectoryInfo(Application.dataPath);
        // string packagesPath = Path.Combine(parentDir.Parent.FullName, "Packages");
        // DirectoryInfo packages = new DirectoryInfo(packagesPath);
        // DirectoryInfo packageList = packages.GetDirectories().Where(x => x.Name.Contains("framework")).FirstOrDefault();
        // string externalScriptsDir = Path.Combine(packageList.FullName, "scripts");

        string externalScriptsDir = Path.Combine(fileInfo.Directory.Parent.FullName, "scripts");

        if (!Directory.Exists(externalScriptsDir))
        {
            externalScriptsDir = Path.Combine(fileInfo.Directory.Parent.FullName, "Scripts");
        }



        string scriptFullPath = Path.Combine(externalScriptsDir, scriptName);

        UnityEngine.Debug.LogError(scriptFullPath + "= script full  path");
        UnityEngine.Debug.LogError(GetArgumentName(scriptFullPath, arguments));

        // Process.Start("powershell.exe", @"""C:\Users\owenm\Documents\GitProjects\Unity\other\scourge-unity-base\Packages\module_framework\scripts\checkPackageValid.ps1"" ""arg""");
        // return;
        Process process = new Process();
        process.StartInfo.FileName = GetCommandName(scriptName);
        UnityEngine.Debug.Log(scriptFullPath);
        if (process.StartInfo.FileName != scriptName)
            process.StartInfo.Arguments = GetArgumentName(scriptFullPath, arguments);
        process.StartInfo.RedirectStandardError = true;
        process.StartInfo.RedirectStandardOutput = true;
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.WorkingDirectory = Path.GetDirectoryName(externalScriptsDir);
        process.StartInfo.UseShellExecute = false;
        process.OutputDataReceived += OutputHandler;
        if (outputHandler != null)
            process.OutputDataReceived += new DataReceivedEventHandler(outputHandler);
        process.ErrorDataReceived += ErrorHandler;
        process.Start();
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        process.WaitForExit();

        if (callback != null)
        {
            callback();
        }
        // process.Kill(); //throws error if already exited
        process.Dispose();
        process.Close();
        UnityEngine.Debug.LogFormat("Finished running {0}", scriptName);
    }

    private static string GetCommandName(string path)
    {
        if (path.EndsWith(".py")) return "python";
        if (path.EndsWith(".ps1"))
        if (Application.platform == RuntimePlatform.LinuxEditor)
        {
            return "pwsh";
        }
        else
        {
            return "powershell";
        }

        else if (path.EndsWith(".bat")) return "cmd";
        else return "sh";
    }

    private static string GetArgumentName(string path, string arguments)
    {
        return @"""" + path + @""" " + arguments; //temporary while im dealing with powershell scripts cba tidying

        string result = path;
        bool justPath = !path.EndsWith(".sh");
        if (!string.IsNullOrWhiteSpace(arguments))
        {
            if (path.EndsWith(".ps1"))
            {
                result = result + "\" \"" + arguments;
            }
            else
            {
                result = result + " " + arguments;
            }
        }
        if (justPath)
        {
            if (path.EndsWith(".ps1"))
            {
                return "-file \"" + result + "\" ";
            }
            return "\"" + result + "\"";
        }
        else return "--login -i " + result;
        // else return "" + path;
    }

    private static void ErrorHandler(object sendingProcess, DataReceivedEventArgs outLine)
    {
        string error = outLine.Data;
        {
            if (!string.IsNullOrEmpty(error))
            {
                if (error != IOCTL_ERROR && error != JOB_CONTROL_ERROR)
                {
                    UnityEngine.Debug.LogError(outLine.Data);
                }
            }
        }
    }

    private static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
    {
        UnityEngine.Debug.Log(outLine.Data);
    }
}
