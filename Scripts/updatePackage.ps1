$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
Write-host "My directory is $dir"


Push-Location $dir

$reponame = $args[0]

Push-Location $reponame

git add .
git commit -m "Automatic commit message"
git push
git push --tags