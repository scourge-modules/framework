$old = $args[0]
$new = $args[1]

Write-Output "old is $old new is $new"

$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
Write-host "My directory is $dir"

Set-Location $dir

Write-Output "renaming tags..."

git tag module/$new module/$old
git tag -d module/$old
git push origin :refs/tags/module/$old
git push --tags

$reponameOld = $old -replace "/", "_"
$reponameNew = $new -replace "/", "_"

Set-Location ..
Set-Location ..
Get-ChildItem
Set-Location $reponameOld
Get-ChildItem



Write-Output "renaming $reponameOld repository to $reponameNew"
$originURL = git config --get remote.origin.url
Write-Output "previous URL was $originURL"
$originURL = $originURL -replace "$reponameOld" , "$reponameNew"
git remote set-url origin $originURL
Write-Output "new URL is $originURL"


#rename references in package.json
((Get-Content -path .\package.json -Raw) -replace "$reponameOld", "$reponameNew") | Set-Content -Path .\package.json


#rename asmdef files - not working
$reponameOld = $reponameOld.substring(0, 1).toupper() + $reponameOld.substring(1).tolower()   
$reponameNew = $reponameNew.substring(0, 1).toupper() + $reponameNew.substring(1).tolower()  

$asmdefFiles = Get-ChildItem . *.asmdef -rec
foreach ($file in $asmdefFiles) {
	(Get-Content $file.PSPath) |
	Foreach-Object { $_ -replace "$reponameOld", "$reponameNew" } |
	Set-Content $file.PSPath
	Rename-Item $file.PSPath $file.Name.replace("$reponameOld", "$reponameNew")
}

# Get-ChildItem -File -Recurse -Include .asmdef | ForEach-Object { ((Get-Content -path $_.PSPath -Raw) -replace "$reponameOld", "$reponameNew") | Set-Content -Path $_.PSPath }
# Get-ChildItem -File -Recurse -Include .asmdef | ForEach-Object { Rename-Item -Path $_.PSPath -NewName $_.Name.replace("$reponameOld", "$reponameNew") }
$reponameOld = $reponameOld.tolower();
$reponameNew = $reponameNew.tolower();



$currentFolderName = (Get-Item .).FullName

Write-Output "Renaming folder $currentFolderName to $reponameNew"
#rename folder - go up a directory so shell is not using this directory
Set-Location ..
Move-Item $currentFolderName $reponameNew -Force
