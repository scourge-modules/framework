#To install this we need Install-Module -Name Atlassian.Bitbucket
#and Install-Module -Name CredentialManager 
# you may also need to update the stored credentials
# username is owen_mcconnell@hotmail.co.uk
#pass is bitbucket pass

$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
Write-host "My directory is $dir"


Push-Location $dir

$reponame = $args[0]

git tag module/$reponame
git push --tags

$reponame = $reponame -replace "/", "_"

Write-Host "Logging in to bitbucket.com..."
$Cred = $(Get-StoredCredential -Target 'www.bitbucket.com')
$OAuth = $(Get-StoredCredential -Target 'www.bitbucket.com:oauth')
Login-Bitbucket -AtlassianCredential $Cred -OAuthConsumer $OAuth 
	
Write-Host "Creating $reponame as a new repository"
New-BitbucketRepository -Team "scourge-modules" -RepoSlug $reponame

# $reponame = $reponame -replace "/", "_"
# should be in another script with the above line added to enable create flows matching
Set-Location ..
Set-Location ..
git clone "git@bitbucket.org:scourge-modules/$reponame.git" $reponame