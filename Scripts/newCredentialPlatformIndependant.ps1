##use Params as input rather than read-host
 param(
      [Parameter(Mandatory = $True,valueFromPipeline=$true)][String] $Target,
      [Parameter(Mandatory = $True,valueFromPipeline=$true)][String] $Comment
      )
##use Var for get-creds to pass creds securely.
$Creds = Get-Credential -Message 'Enter Creds'

##Splatting used for passing property values
$Splat = @{
    Target = $Target
    UserName = $Creds.UserName
    Password = $Creds.Password
    Comment = $Comment
    Persist = 'LocalMachine' ##so the cred is persistent to the local machine and not the current session
}

New-StoredCredential @Splat ##create the cred

Get-StoredCredential -Target $Target ##get the cred

Remove-StoredCredential -Target $Target ##delete the cred

Get-StoredCredential -AsCredentialObject ##get all stored creds in readable list