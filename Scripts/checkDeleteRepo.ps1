$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
Write-host "My directory is $dir"

Push-Location $dir

$reponame = $args[0]
$reponame = $reponame -replace "/", "_"

Set-Location ..
Set-Location ..
Set-Location $reponame

git status